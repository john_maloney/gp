#!/bin/sh
# Build GP for MacOS with statically-linked SDL2, jpeg, cairo, and portaudio libraries
#
# First, build and install the libraries GP depends on:
#
#	cairo, pixman, freetype, fontconfig, libjpeg, libpng, SDL2, and portaudio
#
# Then run this shell script to build and link.
#
# You can use "otool -L mac_gp" to list the library dependencies. That list should include
# only frameworks and standard MacOS libraries (not SDL2 or cairo). The standard libraries
# are (as of Oct 2019): libz, libbz, libiconv, libexpat, libSystem.B, and libobjc.A

gcc -std=c99 -Wall -O3 -mmacosx-version-min=10.6 \
-D NO_CAMERA \
-I/usr/local/include/SDL2 \
cache.c dict.c embeddedFS.c events.c gp.c graphicsPrims.c interp.c jpegPrims.c mem.c memGC.c oop.c \
parse.c prims.c serialPortPrims.c sha1.c sha2.c socketPrims.c soundPrims.c textAndFontPrims.c vectorPrims.c \
/usr/local/lib/libSDL2.a \
/usr/local/lib/libcairo.a \
/usr/local/lib/libpixman-1.a \
/usr/local/lib/libfreetype.a \
/usr/local/lib/libfontconfig.a \
/usr/local/lib/libexpat.a \
/usr/local/lib/libjpeg.a \
/usr/local/lib/libpng.a \
/usr/local/lib/libportaudio.a \
-lz -lbz2 -liconv \
-framework AudioToolBox -framework AudioUnit -framework Carbon -framework Cocoa \
-framework CoreAudio -framework CoreMIDI -framework ForceFeedback -framework IOKit \
-framework CoreVideo \
-o mac_gp
