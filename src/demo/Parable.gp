// Parable of the Polygons stub

to pdemo {
  board = (newBoard 12 12)

  keys = (array 'foo' 'bar' nil)

  for x 12 {
    for y 12 {
      val = (at keys (rand 1 3))
      if (notNil val) {
        p = (newPawn val y x)
        addPart (morph board) (morph p)
      }
    }
  }

  fixLayout board
  addPart (global 'page') board
}


defineClass Pawn morph key row col happy

to newPawn key row col {
  p = (new 'Pawn' nil nil row col true)
  initialize p key
  return p
}

method initialize Pawn keyIndicator {
  if (isNil keyIndicator) {keyIndicator = 'foo'}
  key = keyIndicator
  morph = (newMorph)
  setGrabRule morph 'handle'
  setHandler morph this
  scale = (global 'scale')
  setExtent morph (* 50 scale) (* 50 scale)
}

method redraw Pawn {
  yellow = (color 230 230 0)
  blue = (color 100 100 255)
  red = (color 255 0 100)
  bm = (newBitmap (width morph) (height morph))
  pen = (newPen bm)
  if (key == 'foo')  {
    if happy {
      setColor pen blue
    } else {
      setColor pen (darker red)
    }
    scale = (global 'scale')
    side = (* scale 44)
    inset = (* scale 3)
    fillRect pen inset inset side side
  } (key == 'bar') {
    if happy {
      setColor pen yellow
    } else {
      setColor pen red
    }
    fillIsoscelesTriangle pen (rect 0 0 (width bm) (height bm)) 'up')
  }
  setCostume morph bm
}

method x Pawn {return (hCenter (bounds morph))}
method y Pawn {return (vCenter (bounds morph))}

method aboutToBeGrabbed Pawn {
  board = (handler (owner morph))
  atPut board row col nil
}

method justDropped Pawn hand {
  parent = (handler (owner morph))
  if (not (isClass parent 'Board')) {return}
  targetCol = (col parent (x this))
  targetRow = (row parent (y this))
  dta = (at parent targetRow targetCol)
  if (isNil dta) {
    row = targetRow
    col = targetCol
  }
  snapToGrid this
}

method snapToGrid Pawn {
  board = (handler (owner morph))
  setCenter morph (x board col) (y board row)
  atPut board row col key
  refresh board
}

method neighbors Pawn {
  board = (handler (owner morph))
  result = (list)
  maxC = (cols board)
  maxR = (rows board)
  for r 3 {
    y = ((row - 2) + r)
    if (and (y > 0) (y <= maxR)) {
      for c 3 {
        x = ((col - 2) + c)
        if (and (x > 0) (x <= maxC)) {
          add result (at board y x)
        }
      }
    }
  }
  return result
}

method isHappy Pawn {
  others = 0
  for each (neighbors this) {
    if (and (notNil each) (each != key)) {
      others += 1
      if (others > 2) {return false}
    }
  }
  return true
}


defineClass Costume texture rotationPoint

method texture Costume {return texture}
method rotationPoint Costume {return rotationPoint}

defineClass Board morph grid

to newBoard rows cols {
  b = (new 'Board')
  initialize b rows cols
  return b
}

method initialize Board rows cols {
  grid = (newArray rows)
  for i rows {
    atPut grid i (newArray cols)
  }
  morph = (newMorph)
  setGrabRule morph 'handle'
  setHandler morph this
  scale = (global 'scale')
  setExtent morph (* 60 scale cols) (* 60 scale rows)
}

method redraw Board {
  setCostume morph (newBitmap (width morph) (height morph) (color))
  fixLayout this
}

method fixLayout Board {
  for each (parts morph) {
    snapToGrid (handler each)
  }
}

method refresh Board {
  for each (parts morph) {
    sentiment = (isHappy (handler each))
    if (sentiment != (getField (handler each) 'happy')) {
      setField (handler each) 'happy' sentiment
      redraw (handler each)
    }
    if sentiment {
      setGrabRule each 'ignore'
    } else {
      setGrabRule each 'handle'
    }
  }
}

method at Board row col {return (at (at grid row) col)}
method atPut Board row col data {atPut (at grid row) col data}
method rows Board {return (count (at grid 1))}
method cols Board {return  (count grid)}

method x Board col {
  cWidth = ((width morph) / (cols this))
  return (+ (left morph) (* (col - 1) cWidth) (cWidth / 2))
}

method y Board row {
  cHeight = ((height morph) / (rows this))
  return (+ (top morph) (* (row - 1) cHeight) (cHeight / 2))
}

method col Board x {
  rel = (toInteger (x - (left morph)))
  col = ((width morph) / (cols this))
  return (+ 1 (rel / col))
}

method row Board y {
  rel = (toInteger (y - (top morph)))
  row = ((height morph) / (rows this))
  return (+ 1 (rel / row))
}

method wantsDropOf Board aHandler {return (isClass aHandler 'Pawn')}

