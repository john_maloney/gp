to star {
 array = (array
    250.00000 150.00000 237.65650 183.01064 225.31301 216.02128
    225.31301 216.02128 190.10368 217.55979 154.89434 219.09830
    154.89434 219.09830 182.47498 241.03850 210.05562 262.97871
    210.05562 262.97871 200.63855 296.94020 191.22147 330.90169
    191.22147 330.90169 220.61073 311.45084 250.00000 292.00000
    250.00000 292.00000 279.38926 311.45084 308.77852 330.90169
    308.77852 330.90169 299.36144 296.94020 289.94437 262.97871
    289.94437 262.97871 317.52501 241.03850 345.10565 219.09830
    345.10565 219.09830 309.89631 217.55979 274.68698 216.02128
    274.68698 216.02128 262.34349 183.01064 250.00000 150.00000
  )
  for i (count array) {
    atPut array i ((at array i) - 250)
  }
  return (toFloat32Array array)
}

defineClass Star geometry transform dy da fill

method randomColor Star gezira {
  fill = (init (new (at gezira 'UniformColorFill')))
  color = (color (rand 0 255) (rand 0 255) (rand 0 255) (255 * 0.6))
  setColor fill color
  return fill
}

method next Star ySize {
  y = (m32 transform)
  y += dy
  if ((y - 20) >= ySize) {
    y = -20
  }
  rotateBy transform da
  setTranslation transform (m31 transform) y
}

method renderStar Star canvas {
  setFill canvas fill
  s = (saveState canvas)
  setStroke canvas nil
  transformBy canvas transform
  drawPath canvas geometry
  restoreState canvas s
}

call (function {
  openWindow
  math = (loadModule 'modules/GeziraMath.gp')
  gezira = (loadModule 'modules/GeziraCanvas.gp' math)
  canvas = (init (new (at gezira 'GeziraCanvas')))
  bitmap = (newBitmap 500 500)
  setTarget canvas bitmap
  Matrix = (at math 'Matrix')

  timer = (newTimer)

  stars = (newArray 500)
  data = (star)
  for i (count stars) {
    t = (beIdentity (init (new Matrix)))
    setScale t 0.2
    setTranslation t (rand 0 500) (rand 0 500)
    atPut stars i (new 'Star' data t (((rand 0 200) / 100.0) + 1) (((rand 0 100) - 50) / 200.0))
    randomColor (at stars i) gezira
  }

  msecs = (msecs timer)
  lastRenderCount = 0
  for renderCount 1000 {
    fill bitmap (color 255 255 255)
    setTarget canvas bitmap

    for i (count stars) {
      s = (at stars i)
      renderStar s canvas
      next s 500
    }
    sync canvas

    drawBitmap nil bitmap
    flipBuffer
    now = (msecs timer)
    if ((now - msecs) > 1000) {
      print (renderCount - lastRenderCount)
      msecs = now
      lastRenderCount = renderCount
    }
  }
})

