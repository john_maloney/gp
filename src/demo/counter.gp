// counter demo for morph-handler-renderer architecture

defineClass Counter morph count tally incrementor resetter resizer skin border color

method increment Counter {
  count += 1
  setText tally (toString count)
}

method reset Counter {
  count = 0
  setText tally (toString count)
}

method initialize Counter {
  count = 0
  border = 6
  color = (color 230 230 255)
  skin = 'normal'
  morph = (newMorph this)
  setGrabRule morph 'handle' // draggable
  setMinExtent morph 160 130

  tally = (newText '0' 'Arial Bold' 72)
  addPart morph (morph tally)

  incrementor = (new 'Trigger')
  setData incrementor 'increment'
  setAction incrementor (action 'increment' this)
  setMorph incrementor (newMorph incrementor)
  setRenderer incrementor this
  addPart morph (morph incrementor)

  resetter = (new 'Trigger')
  setData resetter 'reset'
  setAction resetter (action 'reset' this)
  setMorph resetter (newMorph resetter)
  setRenderer resetter this
  addPart morph (morph resetter)

  resizer = (resizeHandle this)
  setExtent morph 200 150
  fixLayout this
}

method fixLayout Counter {
  setPosition (morph tally) ((left (bounds morph)) + border) ((top (bounds morph)) + border)

  clearCostumes incrementor
  clearCostumes resetter
  normal incrementor
  normal resetter

  setPosition (morph incrementor) (left (bounds (morph tally))) ((bottom (bounds (morph tally))) + border)
  setExtent (morph incrementor) (- (- ((width (bounds morph)) / 2) border) (border / 2)) (- (- (height (bounds morph)) (height (bounds (morph tally)))) (border * 3))

  setPosition (morph resetter) ((right (bounds (morph incrementor))) + border) (top (bounds (morph incrementor)))
  setExtent (morph resetter) (width (bounds (morph incrementor))) (height (bounds (morph incrementor)))

  setRight (morph resizer) (right (bounds morph))
  setBottom (morph resizer) (bottom (bounds morph))
}

method redraw Counter {
  w = (width (bounds morph))
  h = (height (bounds morph))
  bm = (newBitmap w h color)
  setCostume morph bm
  fixLayout this
}

method touchHold Counter {
  popUpAtHand (contextMenu this) (handler (root morph))
  return true
}

method contextMenu Counter {
  menu = (menu 'Skin' this)
  addItem menu 'normal...' 'normalSkin'
  addItem menu 'flat...' 'flatSkin'
  return menu
}

method normalSkin Counter {
  skin = 'normal'
  redraw this
}

method flatSkin Counter {
  skin = 'flat'
  redraw this
}

method normalCostume Counter data {
  if (skin == 'normal') {
    return (buttonBitmap data (darker color 40) (width (bounds (morph incrementor))) (height (bounds (morph incrementor))))
  } else {
    bm = (newBitmap (width (bounds (morph incrementor))) (height (bounds (morph incrementor))))
    if (data == 'increment') {
      fill bm (color 100 255 100)
    } else {
      fill bm (color 255 100 100)
    }
    return bm
  }
}

method highlightCostume Counter data {
  if (skin == 'normal') {
    return (buttonBitmap data (darker color 60) (width (bounds (morph incrementor))) (height (bounds (morph incrementor))))
  } else {
    bm = (newBitmap (width (bounds (morph incrementor))) (height (bounds (morph incrementor))))
    if (data == 'increment') {
      fill bm (color 150 255 150)
    } else {
      fill bm (color 255 150 150)
    }
    return bm
  }
}

method pressedCostume Counter data {
  if (skin == 'normal') {
    return (buttonBitmap data (darker color 60) (width (bounds (morph incrementor))) (height (bounds (morph incrementor))) true)
  } else {
    bm = (newBitmap (width (bounds (morph incrementor))) (height (bounds (morph incrementor))))
    if (data == 'increment') {
      fill bm (color 100 200 100)
    } else {
      fill bm (color 200 100 100)
    }
    return bm
  }
}

to demo {
  page = (newPage 1000 800)
  open page 'Counter Demo'
  counter = (new 'Counter')
  initialize counter
  setPosition (morph counter) 50 50
  addPart (morph page) (morph counter)
  startStepping page
}

demo