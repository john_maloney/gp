// HelloServer.gp - A trivial HTTP server that reports uptime and hit count.
//
// To keep things simple, this example takes a number of shortcuts:
//   1. it uses HTTP/1.0, not HTTP/1.1
//   2. the "path" is extracted from first line of the request; the rest is ignored
//   3. requests are handled synchronously

defineClass HelloServer startSecs requestCount

method start HelloServer portNum {
  // start (new 'HelloServer') 1234
  if (isNil portNum) { portNum = 1234 }
  startSecs = (first (time))
  requestCount = 0
  serverLoop this portNum
}

method serverLoop HelloServer portNum {
  serverSocket = (openServerSocket portNum)
  print 'Server listening on port' portNum
  while true {
	clientSock = (acceptConnection serverSocket)
	if (notNil clientSock) {
	  handleConnection this clientSock
	} else {
	  gcIfNeeded this
	  sleep 10
	}
  }
}

method handleConnection HelloServer socket {
  t = (newTimer)
  request = (getRequest this socket)
  if (isNil request) {
	closeSocket socket
	return
  }

  parts = (words (first (lines request)))
  if ((count parts) == 3) {
	path = (at parts 2)
  } else {
	path = ''
  }
  if (path != '/favicon.ico') { requestCount += 1 }

  writeSocket socket (response this path)
  closeSocket socket
  print 'Path:' path (join '(' (msecs t) ' msecs)')
}

method getRequest HelloServer socket {
  t = (newTimer)
  request = ''
  while ((byteCount request) == 0) {
	if ((msecs t) > 100) { return nil } // timeout
	buf = (readSocket socket)
	if (isNil buf) { return nil }
	if ((byteCount buf) > 0) {
	  request = (join request buf)
	} else {
	  sleep 5 // no data; wait a bit
	}
  }
  return request
}

method response HelloServer path {
  crlf = (string 13 10)
  body = (join
'<html>
<body>
<h1>Hello from GP!</h1>
<p>Requests: ' requestCount
'<br>Uptime: ' (timeSince startSecs)
'</body>
</html>
')

  return (join
'HTTP/1.0 200 OK
Content-Type: text/html
Content-Length: ' (byteCount body)
	crlf
	crlf
	body)
}

method gcIfNeeded HelloServer {
  wordsAllocated = (at (memStats) 4)
  if (wordsAllocated > 1000000) { gc }
}

start (new 'HelloServer') 1234
