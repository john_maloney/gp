to demo {
  page = (newPage 1000 800)
  open page 'GP Inspector'
  ins = (inspectorOn page)
  setPosition (morph ins) 10 10
  addPart (morph page) (morph ins)
  startStepping page
}

demo