// iosOps.h - Functions to support iOS
// John Maloney, October 2016

OBJ ios_absolutePath(char *path);
void ios_fullPath(char *gpPath, char *result, int resultSize);
OBJ ios_directoryContents(char *dirName, gp_boolean listDirectories);
void ios_loadLibrary();
void ios_loadUserFilesAndMainFile();

// deprecated:
void ios_hideStatusBar();
