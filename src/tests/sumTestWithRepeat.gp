to sumTestWithRepeat {
  timer = (newTimer)
  sum = 0
  one = 1
  i = 1
  repeat 10000000 {
    sum += one
    i += 1
  }
  log (msecs timer) 'msecs'
}

sumTestWithRepeat
