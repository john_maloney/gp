call (function {
  addModDef = '
module
  moduleVariables E
  moduleExports Node

  defineClass Node left right

  method left Node { return left }
  method right Node { return right }

  method eval Node {
	if (isNumber left) {
	  l = left
	} else {
	  l = (eval left)
	}
	if (isNumber right) {
	  r = right
	} else {
	  r = (eval right)
	}
	return (add (shared ''E'') l r)
  }

  defineClass Evaluator

  method add Evaluator args {
	val = 0
	for i ((argCount) - 1) {
	  val = (val + (arg (i + 1)))
	}
	return val
  }

  // construct an expander on a computed class
//   method foo (getField (classes) 6) {
// 	return (count this)
//   }

  to initializeModule {
	setShared ''E'' (new (class ''Evaluator''))
  }
'

  mulModDef = '
module
  moduleVariables E
  moduleExports Node

  defineClass Node left right

  method left Node { return left }
  method right Node { return right }

  method eval Node {
	if (isNumber left) {
	  l = left
	} else {
	  l = (eval left)
	}
	if (isNumber right) {
	  r = right
	} else {
	  r = (eval right)
	}
	return (times (shared ''E'') l r)
  }

  defineClass Evaluator

  method times Evaluator args {
	val = 1
	for i ((argCount) - 1) {
	  val = (val * (arg (i + 1)))
	}
	return val
  }

  to initializeModule {
	setShared ''E'' (new (class ''Evaluator''))
  }
'

  addMod = (initialize (new 'Module') 'Add')
  loadModuleFromString addMod addModDef
  callInitializer addMod

  mulMod = (initialize (new 'Module') 'Mult')
  loadModuleFromString mulMod mulModDef
  callInitializer mulMod

  Add = (at addMod 'Node')
  Mul = (at mulMod 'Node')

  tree = (new Add (new Add 3 (new Mul 4 5)) 2)
  val = (eval tree)
  assert val 25 '(3 + (4 * 5)) + 2'
})
