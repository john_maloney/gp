assert (className (class 'String')) 'String' 'String name'

assert (canChange (class 'String')) false 'canChange String'
assert (canChange (class 'LargeInteger')) false 'canChange LargeInteger'

defineClass Class1 a b c

assert (className (class 'Class1')) 'Class1' 'Class1 name'

assert ((classIndex (class 'Class1')) > (classIndex (class 'LargeInteger'))) true
assert (canChange (class 'Class1')) true 'canChange Class1'

method mth1 Class1 d e {
  return (d + e)
}

assert (argNames (methodNamed (class 'Class1') 'mth1')) (array 'this' 'd' 'e') 'argNames'
assert (varReferenced (class 'Class1') 'd') true
assert (varReferenced (class 'Class1') 'e') true
assert (varReferenced (class 'Class1') 'f') false

call (function {
  addField (class 'Class1') 'f'
  assert (fieldNames (class 'Class1')) (array 'a' 'b' 'c' 'f') 'added field f'

  cls = (class 'Class1')
  assert (fieldNames cls) (array 'a' 'b' 'c' 'f') 'check it with another reference to class'

  (deleteField (class 'Class1') 'f')
  assert (varReferenced (class 'Class1') 'f') false 'removed field f'
  assert (fieldNames (class 'Class1')) (array 'a' 'b' 'c') 'deleted field f'
  assert (fieldNames cls) (array 'a' 'b' 'c') 'check it with another reference to class'
})

(deleteField (class 'Class1') 'a')

assert (varReferenced (class 'Class1') 'a') false
assert (fieldNames (class 'Class1')) (array 'b' 'c') 'final Class1 field names'
