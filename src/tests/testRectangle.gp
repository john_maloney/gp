to testRectangle {
  assert (left (rect 4 5 6 7)) 4 'left'
  assert (top (rect 4 5 6 7)) 5 'top'
  assert (width (rect 4 5 6 7)) 6 'width'
  assert (height (rect 4 5 6 7)) 7 'height'
  assert (right (rect 4 5 6 7)) 10 'right'
  assert (bottom (rect 4 5 6 7)) 12 'bottom'

  r = (rect)
  setLeft r 4
  assert (left r) 4 'setLeft'
  setTop r 5
  assert (top r) 5 'setTop'
  setWidth r 6
  assert (width r) 6 'setWidth'
  setHeight r 7
  assert (height r) 7 'setHeight'
  setRight r 8
  assert (right r) 8 'setRight'
  setBottom r 9
  assert (bottom r) 9 'setBottom'
  
  assert ((rect 4 5 6 7) == (rect 4 5 6 7)) true '=='
  assert ((rect 4 5 6 7) == 'foo') false '== not (type)'
  assert ((rect 4 5 6 7) == (rect 3 5 6 7)) false '== not (left)'
  assert ((rect 4 5 6 7) == (rect 4 3 6 7)) false '== not (top)'
  assert ((rect 4 5 6 7) == (rect 4 5 3 7)) false '== not (width)'
  assert ((rect 4 5 6 7) == (rect 4 5 6 3)) false '== not (height)'
  assert ((rect 4 5 6 7) != (rect 4 5 6 7)) false '!='
  assert ((rect 4 5 6 7) != 'foo') true '!= (type)'
  assert ((rect 4 5 6 7) != (rect 3 5 6 7)) true '!= (left)'

  r = (rect 10 10 10 10 )
  assert (insetBy r 2) (rect 12 12 6 6) 'insetBy (single dimension)'
  assert (insetBy r 2 3) (rect 12 13 6 4) 'insetBy (double dimension)'
  assert (expandBy r 2) (rect 8 8 14 14) 'expandBy (single dimension)'
  assert (expandBy r 2 3) (rect 8 7 14 16) 'expandBy (double dimension)'

  assert (intersection (rect 5 5 20 20) (rect 10 10 20 20)) (rect 10 10 15 15) 'intersection'
  assert (mergedWith (rect 5 5 20 20) (rect 10 10 20 20)) (rect 5 5 25 25) 'mergedWith'

  r = (rect 0 0 20 20)
  intersect r (rect 10 10 20 20)
  assert r (rect 10 10 10 10) 'intersect left to right'
  r = (rect 10 10 20 20)
  intersect r (rect 0 0 20 20)
  assert r (rect 10 10 10 10) 'intersect right to left'

  r = (rect 0 0 10 10)
  merge r (rect 20 20 10 10)
  assert r (rect 0 0 30 30) 'merge left to right'
  r = (rect 20 20 10 10)
  merge r (rect 0 0 10 10)
  assert r (rect 0 0 30 30) 'merge right to left'

  assert (containsPoint (rect 5 5 5 5) 7 8) true 'containsPoint 7 8'
  assert (containsPoint (rect 5 5 5 5) 5 5) false 'containsPoint 5 5'
  assert (containsPoint (rect 5 5 5 5) 10 10) true 'containsPoint 10 10'
  assert (containsPoint (rect 5 5 5 5) 7 3) false 'containsPoint not (y)'
  assert (containsPoint (rect 5 5 5 5) 3 8) false 'containsPoint not (x)'
  assert (containsPoint (rect 5 5 5 5) 3 11) false 'containsPoint not (x y)'

  assert (intersects (rect 5 5 10 10) (rect 10 10 10 10 )) true 'intersects'
  assert (intersects (rect 5 5 10 10) (rect 0 0 10 10 )) true 'intersects'
  assert (intersects (rect 5 5 10 10) (rect 8 8 5 5 )) true 'intersects'
  assert (intersects (rect 5 5 10 10) (rect 0 0 20 20 )) true 'intersects'

  assert (intersects (rect 5 5 10 10) (rect 20 5 10 10 )) false 'intersects not'
  assert (intersects (rect 5 5 10 10) (rect 2 2 10 2 )) false 'intersects not'
  assert (intersects (rect 5 5 10 10) (rect 2 2 2 5 )) false 'intersects not'
  assert (intersects (rect 5 5 10 10) (rect 2 20 2 5 )) false 'intersects not'

  assert (translatedBy (rect 1 2 3 4) 5) (rect 6 7 3 4) 'translateBy (single dimension)'
  assert (translatedBy (rect 1 2 3 4) 5 6) (rect 6 8 3 4) 'translateBy (double dimensions)'
}

testRectangle

