defineClass TestLoopLocalVar count

method innerWhileLoop TestLoopLocalVar {
  i = 0
  while (i < 10) {
    count += 1
	i += 1
  }
}

method whileTest TestLoopLocalVar {
  i = 0
  while (i < 10) {
    innerWhileLoop this
    i += 1
  }
}

method innerForLoop TestLoopLocalVar {
  for i 10 { count += 1 }
}

method forTest TestLoopLocalVar {
  for i 10 { innerForLoop this }
}

method run TestLoopLocalVar {
  count = 0
  whileTest this
  assert count 100 'while loop local var'

  count = 0
  forTest this
  assert count 100 'for loop local var'
}

run (new 'TestLoopLocalVar')
