to primes1000 {
 timer = (newTimer)
 flags = (newArray 8190)
 repeat 1000 {
  primeCount = 0
  fillArray (v flags) true
  i = 2
  repeat 8188 {
    if (at (v flags) (v i)) {
      primeCount += 1 // found a prime
      j = (2 * (v i))
      while (< (v j) 8190) { // mark multiples of i as non-prime
	atPut (v flags) (v j) false
	j += (v i)
      }
    }
    i += 1
  }
 }
 log (msecs timer) 'msecs'
}

primes1000
