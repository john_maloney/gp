call (function {

  in = (toBinaryData (array))
  out = (base64Decode (base64Encode in))
  if (out != in) { print 'base64 encode/decode failed for zero-length input' }

  repeat 1000 {
	in = (newArray (rand 1 20))
	for i (count in) {
	  atPut in i (rand 0 255)
	}
	in = (toBinaryData in)
	out = (base64Decode (base64Encode in))
	if (out != in) { print 'base64 encode/decode failed:' ((byteCount in) % 3) }
  }
  print 'ok'
})
