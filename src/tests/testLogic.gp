call (function {
x = 1
y = 2

// ----- AND -----

assert (and (x > 0) (y > 0)) true '(and (x > 0) (y > 0))'
assert (and (x > 0) (y > 2)) false '(and (x > 0) (y > 2))'

q = (array 1)

// expect (and (y > 0) (error)) error
assert (and false (atPut q 1 2)) false '(and false (atPut q 1 2))'
assert (and (y > 2) (atPut q 1 3)) false '(and (y > 2) (atPut q 1 3))'
assert (at q 1) 1 '(at q 1)' // shortcircuit AND should not have changed q

// ----- OR -----

assert (or (x > 0) (y > 0)) true '(or (x > 0) (y > 0))'
assert (or (x > 0) (y > 2)) true '(or (x > 0) (y > 2))'

q = (array 1)

//expect (or (y > 2) (error)) error
assert (or true (atPut q 1 2)) true '(or true (atPut q 1 2))'
assert (or (y > 0) (atPut q 1 3)) true '(or (y > 0) (atPut q 1 3))'
assert (at q 1) 1 '(at q 1)'  // shortcircuit OR should not have changed q

// ----- NOT -----

assert (not true) false '(not true)'
assert (not false) true '(not false)'

assert (not (1 == 1)) false '(not (1 == 1))'
assert (not (1 == 2)) true '(not (1 == 2))'
})
