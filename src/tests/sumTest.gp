to sumTest {
  timer = (newTimer)
  sum = 0
  one = 1
  i = 1
  while (< i 10000000) {
    sum += one
    i += 1
  }
  log (msecs timer) 'msecs'
}

sumTest
