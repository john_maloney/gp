call (function {
  addModDef = '
module
  moduleExports Node String test1 test2 test3

  defineClass Node left right
  defineClass String

  to test1 obj {
    return (isClass obj ''Node'')
  }
  to test2 obj {
    return (isClass obj ''String'')
  }
  to test3 obj {
    return (isClass obj ''FooBar'')
  }
'

  mulModDef = '
module
  moduleExports Node test2
  defineClass Node left right
  to test2 obj {
    return (isClass obj ''String'')
  }
'

  addMod = (loadModuleFromString (initialize (new 'Module')) addModDef)
  mulMod = (loadModuleFromString (initialize (new 'Module')) mulModDef)

  Add = (at addMod 'Node')
  Mul = (at mulMod 'Node')

  add = (new Add)
  mul = (new Mul)
  myStr = (new (at addMod 'String'))

  assert (call (at addMod 'test1') add) true    '(isClass m1 m1)'
  assert (call (at addMod 'test1') mul) false   '(isClass m2 m1)'
  assert (call (at addMod 'test1') 'Foo') false '(isClass t1 m1)'
  assert (isClass 'Foo' 'Node') false            '(isClass t1 nil)'
  assert (isClass add 'Node') false              '(isClass m1 nil)'

  assert (call (at addMod 'test2') add) false   '(isClass m1 m2)'
  assert (call (at addMod 'test2') 'Foo') false '(isClass t1 m2)'
  assert (call (at addMod 'test2') myStr) true  '(isClass m1 m1)'
  assert (call (at mulMod 'test2') myStr) false '(isClass m1 m2)'
  assert (call (at mulMod 'test2') 'Foo') true  '(isClass t1 t1)'
  assert (isClass myStr 'String')            false '(isClass m1 t1)'

  assert (call (at addMod 'test3') 'Foo') false '(isClass t1 nil)'
  assert (call (at addMod 'test3') add)   false '(isClass m1 nil)'
  assert (call (at addMod 'test3') myStr) false '(isClass m1 nil)'
  assert (isClass myStr 'FooBar')   false           '(isClass m1 nil)'
})
