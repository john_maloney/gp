// Morphic text demo

to textDemo {

lims = (array

'A programming genius called Hugh
Said "I really must see if it''s true."
So he wrote a routine
To ask "What''s it all mean?"
But the answer was still "42".'

'A programming genius called HEAP
Had trouble in getting to sleep
So he made his lambs troup
through a huge FOR-NEXT loop
FOR 1 TO 10000: NEXT sheep.'

'if(computer.fail==true){
background.setColor(blue);
user.frown();
sys.shutdown();
user.scream("OH, F-- YOU");}'

)

  page = (newPage 600 400)
  open page

  t = (newText (at lims 1) 'Baskerville' 18 (color) 'right')
  setGrabRule (morph t) 'handle'
  setEditRule t 'editable'
  setPosition (morph t) 20 20
  addPart (morph page) (morph t)

  t = (newText (at lims 2) 'Verdana Bold' 10 (color 120 120 250) 'center' (color 0 0 100) 1)
  setGrabRule (morph t) 'handle'
  setEditRule t 'editable'
  setPosition (morph t) 350 130
  addPart (morph page) (morph t)

  t = (newText (at lims 3) 'AmericanTypewriter' 24 (color 240 100 60) 'left' (color 100 0 0) -1)
  // setGrabRule (morph t) 'handle'
  setEditRule t 'code'
  setAlpha (morph t) 180
  setPosition (morph t) 120 220
  addPart (morph page) (morph t)

  startStepping page
}

textDemo