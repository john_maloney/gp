// tests stand-alone Morphs without handlers

to test {
  page = (newPage 600 400)
  open page
  for i 250 {
    m = (newMorph)
    setCostume m (newBitmap (rand 10 100) (rand 10 100) (randomColor))
    setPosition m (rand 1 500) (rand 1 400)
    addSchedule page (schedule (action 'moveBy' m (rand -5 5) (rand -5 5)) (rand 0 100) -1)
    addPart (morph page) m
  }
  startStepping page
}

test
