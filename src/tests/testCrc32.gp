call (function {
  data1 = 'The quick brown fox jumps over the lazy dog'

  // CRC-32 tests
  assert (toStringBase16 (crc '')) '0'
  assert (toStringBase16 (crc 'abcdef')) '4B8E39EF'
  assert (toStringBase16 (crc '123456789')) 'CBF43926'
  assert (toStringBase16 (crc 'ABC')) 'A3830348'
  assert (toStringBase16 (crc data1)) '414FA339'

  // Adler-32 tests
  assert (toStringBase16 (crc '' true)) '1'
  assert (toStringBase16 (crc 'a' true)) '620062'
  assert (toStringBase16 (crc 'abc' true)) '24D0127'
  assert (toStringBase16 (crc 'abcdefghijklmnopqrstuvwxyz' true)) '90860B20'
})
