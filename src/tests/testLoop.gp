call (function {
count = 0
repeat 1000000 {
	count += 1
}

assert count 1000000 'count'

sum = 0
triangle = 0
one = 1
i = 1
while (i <= 10) {
	sum = (sum + one)
	triangle = (triangle + sum)
	i += 1
}

assert i 11 'i'
assert sum 10 'sum'
assert triangle 55 'triangle'

sum = 0
i = 1
repeat 1000000 {
	sum += 1
	i += 1
}

assert i 1000001 'i'
assert sum 1000000 'sum'

//---------------------
sum = 0
i = 1
repeat 0 {
  sum += i
}

assert sum 0 'sum of zero'
})
