to testMorphHierarchy {
  r = (newMorph)
  b1 = (newMorph)
  b2 = (newMorph)
  t1 = (newMorph)
  t2 = (newMorph)
  t3 = (newMorph)
  t4 = (newMorph)
  t5 = (newMorph)
  l1 = (newMorph)
  l2 = (newMorph)
  l3 = (newMorph)
  l4 = (newMorph)
  l5 = (newMorph)
  l6 = (newMorph)
  l7 = (newMorph)
  l8 = (newMorph)


  addPart t1 l1
  addPart t1 l2
  addPart t1 l3
  addPart t1 l4

  assert (count (parts t1)) 4 'addPart count'
  assert (owner l1) t1 'owner'
  assert (parts t1) (toList (array l1 l2 l3 l4)) 'parts'
  assert (root l3) t1 'root'
  assert (allMorphs l1) (toList (array l1)) 'allMorphs'
  assert (allMorphs t1) (toList (array t1 l1 l2 l3 l4)) 'allMorphs'
  assert (allOwners l1) (toList (array l1 t1)) 'allOwners'

  removePart t1 l3
  assert (parts t1) (toList (array l1 l2 l4)) 'removePart'

  addPart r b1
    addPart b1 t1
      // l1
      // l2
      // l4
    addPart b1 t2
      addPart t2 l4
      addPart t2 l5
  addPart r b2
    addPart b2 t3
      addPart t3 l6
    addPart b2 t4
    addPart b2 t5
      addPart t5 l7
      addPart t5 l8

  assert (root l8) r 'root l8'
  assert (root l2) r 'root l2'
  assert (root t5) r 'root t5'
  assert (root b2) r 'root b2'
  assert (allOwners l8) (toList (array l8 t5 b2 r)) 'allOwners l8'
  assert (count (allMorphs r)) 15 'allMorphs r count'
}

testMorphHierarchy

