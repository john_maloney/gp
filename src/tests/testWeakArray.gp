call (function {
  weak = (newIndexable 'WeakArray' 4)

  obj1 = (newArray 10)
  setField weak 1 obj1

  obj2 = true
  setField weak 2 obj2

  obj3 = weak
  setField weak 3 obj3

  gc
  assert (getField weak 1) obj1
  assert (getField weak 2) true
  assert (getField weak 3) weak

  obj1 = nil
  obj2 = nil
  obj3 = nil

  gc
  assert (getField weak 1) nil
  assert (getField weak 2) true
  assert (getField weak 3) weak
})
