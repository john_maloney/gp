call (function {
to foo {
  return ((arg 1) - (arg 2))
}

assert (callWith 'foo' (array 3 4)) -1

to bar {
   return ((arg 1) * (callWith 'foo' (array (arg 2) (arg 3))))
}

assert (callWith 'bar' (array 3 4 5)) -3

assert (callWith '*' (array 3 4 5)) 60
})
