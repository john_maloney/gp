// resizing demo

to demo {
  page = (newPage 800 800)
  open page

  hdl = (newBox nil (color 255 200 200) 0 0 false)
  setExtent (morph hdl) 100 100
  setGrabRule (morph hdl) 'handle'
  setPosition (morph hdl) 50 50
  addPart (morph page) (morph hdl)
  resizeHandle hdl 'horizontal'

  hdl = (newBox nil (color 200 255 200) 0 0 false)
  setExtent (morph hdl) 100 100
  setGrabRule (morph hdl) 'handle'
  setPosition (morph hdl) 50 170
  addPart (morph page) (morph hdl)
  resizeHandle hdl 'vertical'

  hdl = (newBox nil (color 200 200 255) 0 0 false)
  setExtent (morph hdl) 100 100
  setGrabRule (morph hdl) 'handle'
  setPosition (morph hdl) 170 170
  addPart (morph page) (morph hdl)
  resizeHandle hdl

  startStepping page
}

demo
