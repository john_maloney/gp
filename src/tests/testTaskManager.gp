defineClass ClassManagerTest history

method runTask ClassManagerTest msg waitTime {
  for i 10 {
	waitMSecs waitTime
    history = (join history msg)
  }
}

method test1 ClassManagerTest {
  history = ''
  tm = (newTaskMaster)
  addTask tm (newTask (action 'runTask' this 'A' 24))
  addTask tm (newTask (action 'runTask' this 'B' 50))
  stepAllTasksUntilDone tm
  return history
}

assert (test1 (new 'ClassManagerTest')) 'AABAABAABAABAABBBBBB' 'task interleaving with waitTime'
