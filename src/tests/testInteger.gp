assert (highBit 20) 5 'highBit 20'
assert (highBit 140) 8 'highBit 140'
assert (highBit 2730) 12 'highBit 2730'
assert (highBit 699050) 20 'highBit 699050'

assert (toString 699050) '699050' 'toString 699050'
assert (toString -2730) '-2730' 'toString -2730'
assert (toString 0) '0' 'toString 0'

