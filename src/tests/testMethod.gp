method isNilTest Nil {return true}

defineClass Point3D x y z
method isNilTest Point3D {return false}
method self Point3D {return this}
method sum Point3D {return (+ x y z)}
method x Point3D {return x}

call (function {
  p = (new 'Point3D' 1 2 3)
  assert (isNilTest nil) true '(isNilTest nil)'
  assert (isNilTest p) false '(isNilTest p)'
  assert (self p) p '(self p)'
  assert (x p) 1 '(x p)'
  assert (sum p) 6 '(sum p)'
})
