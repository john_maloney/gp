call (function {
a = 0
a += 1
assert a 1 'a'

assert (abs -7) 7 '(abs -7)'
assert (abs 7) 7 '(abs 7)'

result = true
repeat 100000 {
  a = (rand 0 100)
  result = (and result (a >= 0) (a <= 100))
}

assert result true 'result'
})
