to playNoise left right {
  print 'noise left:' left 'right:' right
  openAudio 2048 true
  sleep 5
  bufSize = (samplesNeeded)
  a = (newArray bufSize)
  repeat 10 {
    i = 1
    while (i < bufSize) {
      atPut a i (left * (rand -100 100))
	  atPut a (i + 1) (right * (rand -100 100))
      i += 2
    }
    while ((samplesNeeded) == 0) { sleep 1 }
    writeSamples a
  }
  sleep 50
  closeAudio
}

playNoise 20 20
playNoise 30 0
playNoise 0 30
playNoise 5 5
playNoise 20 20
playNoise 40 40

to playFile fileName {
  print 'WAV file:' fileName
  data = (samples (readWAVFile fileName))
  sampleCount = (count data)
  i = 1
  openAudio 2048
  sleep 5
  bufSize = (samplesNeeded)
  buf = (newArray bufSize)
  while (i < sampleCount) {
    for j bufSize {
	  if (i <= sampleCount) {
	    atPut buf j (at data i)
	    if ((j % 2) == 0) { i += 1 } // downsample
	  } else {
	    atPut buf j 0
	  }
	}
    while ((samplesNeeded) == 0) { sleep 1 }
	writeSamples buf
  }
  sleep 50
  closeAudio
}

playFile 'tests/GuitarStrum.wav'
