// test tab bars

defineClass Person firstName lastName
method firstName Person {return firstName}
method lastName Person {return lastName}
method fullName Person {return (join firstName ' ' lastName)}

to demo {
  page = (newPage 800 700)
  open page true

  people = (array
    (new 'Person' 'John' 'Doe')
    (new 'Person' 'John' 'Maloney')
    (new 'Person' 'Alan' 'Kay')
    (new 'Person' 'Jens' 'Mönig')
    (new 'Person' 'Jens' 'Lincke')
    (new 'Person' 'Alan' 'Borning')
  )

  peopleList = (listBox people 'firstName')
  setPosition (morph peopleList) 20 20
  addPart page peopleList

  detail = (newText)
  setPosition (morph detail) (+ (right (bounds (morph peopleList))) 50) 20
  addPart page detail

  lastNamesList = (tabBar people 'lastName' nil nil 24)
  setPosition (morph lastNamesList) (+ (right (bounds (morph peopleList))) 200) 20
  addPart page lastNamesList

  onSelect peopleList (action 'showDetails' detail)
  onSelect lastNamesList (action 'select' peopleList)
  startStepping page
}

to showDetails aText aPerson {
  setText aText (fullName aPerson)
}

demo
