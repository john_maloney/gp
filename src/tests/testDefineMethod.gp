defineClass Foo a b c

method foo Foo {
  c = (a + b)
  return c
}

method bar Foo {
  c = (a - b)
  return c
}

assert (count (getField (class 'Foo') 'methods')) 2 'method count'

defineClass Foo d e

assert (count (getField (class 'Foo') 'methods')) 2 'method count'

defineClass Foo

method baz Foo {
  c = (d * e)
  return c
}

call (function {
v = (new 'Foo' 3 4 5 6 7)
assert (baz v) 42 'still has fields.'
})

