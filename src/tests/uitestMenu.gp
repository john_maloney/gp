// test and demonstrate menus

to menuDemo {
  page = (newPage 350 250)
  open page

  b = (pushButton 'GP' (color 130 130 130) (action 'actionsMenu' page))
  setPosition (morph b) 50 50
  addPart (morph page) (morph b)

  b = (pushButton 'choices' (color 130 130 130) (action 'choicesMenu' page))
  setPosition (morph b) 150 50
  addPart (morph page) (morph b)

  startStepping page
}

to actionsMenu aPage {
  menu = (menu 'GP' aPage)
  addItem menu 'system version' 'showVersion'
  addLine menu
  addItem menu 'memory usage' 'showMem'
  addItem menu 'language overview' 'showHelp'
  addItem menu 'garbage collect' 'garbageCollect'
  addLine menu
  addItem menu 'quit' 'quitGP'
  popUpAtHand menu aPage
}

to showVersion aPage {inform aPage (at (version) 1)}

to showMem aPage {
  stats = (memStats)
  inform aPage (join (toString (at stats 1)) ' words used out of ' (toString (at stats 2)))
}

to showHelp aPage {inform aPage (help)}
to garbageCollect aPage {inform aPage (toString (gc))}

to quitGP {exit}

to choicesMenu aPage {
  menu = (menu 'Choices' (action 'inform' aPage) true)
  addItem menu 'foo' '"foo" was pressed'
  addItem menu 'bar & baz' 'you said "bar & baz"'
  addItem menu 'qux' 'whatever "qux" is...'
  addLine menu
  addItem menu 'garply' '"garply" it is'
  popUpAtHand menu aPage
}

menuDemo
