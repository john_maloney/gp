call (function {
  b = (newBinaryData 16)
  assert (float32At b 1) 0.0 'initialized to zero'
  assert (classOf (float32At b 1)) (class 'Float') 'class of result'

  float32AtPut b 1 1.5
  float32AtPut b 5 2.75
  float32AtPut b 9 3.14
  float32AtPut b 13 4.1
  //expect (float32AtPut b 16 5.9) error

  assert (float32At b 1) 1.5 'floatAt 1'
  assert (float32At b 5) 2.75 'floatAt 2'

  assert (truncate (* 100 (float32At b 9))) 314 'floatAt 3'
  assert (truncate (* 100 (float32At b 13))) 409 'floatAt 4'
  // expect (float32At b 16) error

  b = (newBinaryData 18)

  float32AtPut b 2 1.5
  float32AtPut b 6 2.75
  float32AtPut b 10 3.14
  float32AtPut b 14 4.1

  assert (float32At b 2) 1.5 'floatAt 1 non-aligned'
  assert (float32At b 6) 2.75 'floatAt 2 non-aligned'

  assert (truncate (* 100 (float32At b 10))) 314 'floatAt 3 non-aligned'
  assert (truncate (* 100 (float32At b 14))) 409 'floatAt 4 non-aligned'

  b = (array 0.5 1.5 2.5 3.5 4.5)

  c = (toFloat32Array b)
  assert (count c) 5 'count of float array'
  assert (at c 2) 1.5 'access an element float array'

  c = (callWith 'float32Array' b)
  assert (count c) 5 'count of float array'
  assert (at c 2) 1.5 'access an element float array'

  b = (newBinaryData 9)
  assert (byteCount b) 9 'byteCount'
  for i 9 {
    byteAtPut b i (i * 10)
  }
  for i 9 {
    assert (byteAt b i) (i * 10) 'uint 8 access'
  }
})

call (function {
  a = (newBinaryData 10)
  uint32AtPut a 1 305419896 // 0x12345678
  uint32AtPut a 5 (((toLargeInteger 128) << 24) | 3430008) // = 0xFF000000 | 0x345678

  assert (uint32At a 1) 305419896 'aligned small integer'
  assert ((uint32At a 5) >> 24) 128 'aligned large integer'
  assert ((uint32At a 5) & 16777215) 3430008 'aligned large integer'

  uint32AtPut a 3 305419896
  uint32AtPut a 7 (((toLargeInteger 128) << 24) | 3430008)

  assert (uint32At a 3) 305419896 'non-aligned small integer'
  assert ((uint32At a 7) >> 24) 128 'non-aligned large integer'
  assert ((uint32At a 7) & 16777215) 3430008 'non-aligned large integer'

  b = (new 'UInt32Array' (newBinaryData 8))
  atPut b 1 100
  atPut b 2 200

  assert (at b 1) 100
  assert (at b 2) 200

})

call (function {
  a = (new 'UInt32Array' (newBinaryData 8))
  b = (new 'UInt32Array' (newBinaryData 8))
  atPut a 1 100
  atPut a 2 200
  atPut b 1 300
  atPut b 2 400

  c = (join a b)

  assert (at c 1) 100
  assert (at c 2) 200
  assert (at c 3) 300
  assert (at c 4) 400
})

call (function {
  buf = (newBinaryData 4)

  intAtPut buf 1 17 false
  assert (intAt buf 1 false) 17
  assert (toArray buf) (array 17 0 0 0)
  intAtPut buf 1 17 true
  assert (intAt buf 1 true) 17
  assert (toArray buf) (array 0 0 0 17)

  uint32AtPut buf 1 17 false
  assert (uint32At buf 1 false) 17
  assert (toArray buf) (array 17 0 0 0)
  uint32AtPut buf 1 17 true
  assert (uint32At buf 1 true) 17
  assert (toArray buf) (array 0 0 0 17)

  epsilon = 0.000001
  float32AtPut buf 1 3.14 false
  assert ((abs ((float32At buf 1 false) - 3.14)) < epsilon) true
  assert (toArray buf) (array 195 245 72 64)
  float32AtPut buf 1 3.14 true
  assert ((abs ((float32At buf 1 true) - 3.14)) < epsilon) true
  assert (toArray buf) (array 64 72 245 195)
})
