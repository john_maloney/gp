// Interpreter State

OBJ globals;
OBJ functions;

// Actions and Inlined Prims

#define STOP 0
#define IF 1
#define REPEAT 2
#define WHILE 3
#define CALL 4
#define RETURN 5
#define GETVAR 6
#define SETVAR 7
#define CHANGEBY 8
#define GETARG 9
#define EVAL_ARG 10
#define NOOP 11

// Function Declarations

OBJ dictAssociationAt(OBJ dict, OBJ key);
OBJ dictAt(OBJ dict, OBJ key);
OBJ dictAtPut(OBJ dict, OBJ key, OBJ newValue);

void flushEvents();
void initGP();
void initGraphics();
int lookup(OBJ primName);
int lookupGraphicsPrim(char *primName);
void printObj(OBJ obj);
void printlnObj(OBJ obj);
void run(OBJ script);
void stop();

// Inlined Functions

inline int evalInt(OBJ obj) {
	if (isInt(obj)) return obj2int(obj);
	printf("evalInt got non-integer: ");
	printlnObj(obj);
	return 0;
}

static inline int intArg(int i, int defaultValue, OBJ b, OBJ args[]) {
	int nargs = argCount(b);
	if ((i < 0) || (i >= nargs)) return defaultValue;
	return isInt(args[i]) ? obj2int(args[i]) : defaultValue;
}

static inline char * strArg(int i, char *defaultValue, OBJ b, OBJ args[]) {
	int nargs = argCount(b);
	if ((i < 0) || (i >= nargs)) return defaultValue;
	OBJ arg = args[i];
	return NOT_CLASS(arg, StringClass) ? defaultValue : obj2str(arg);
}

static inline int clip(int n, int min, int max) {
	if (n < min) return min;
	if (n > max) return max;
	return n;
}

// Interpreter Testing

OBJ primArrayAt(OBJ b, OBJ args[]);
OBJ primArrayAtPut(OBJ b, OBJ args[]);
OBJ primArrayAtAllPut(OBJ b, OBJ args[]);
OBJ primLess(OBJ b, OBJ args[]);
OBJ primMul(OBJ b, OBJ args[]);
OBJ primNewArray(OBJ b, OBJ args[]);

void primeSieves1(int iters);
void primeSieves2(int iters);
void primeSieves3(int iters);

// High Resolution Timer (Mac OS X Only)

#include <mach/mach_time.h>

double conversion_factor;
uint64_t machStartTime;

inline void initNanosecTimer() {
	mach_timebase_info_data_t timebase;
	mach_timebase_info(&timebase);
	conversion_factor = (double) timebase.numer / (double) timebase.denom;
}

inline void nanosecsReset(void) { machStartTime = mach_absolute_time(); }

inline double nanosecs(void) {
	uint64_t now = mach_absolute_time();
	double nanoSecs = (double) (now - machStartTime) * conversion_factor;
	machStartTime = mach_absolute_time();
	return nanoSecs;
}
