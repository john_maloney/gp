// wordCodeTest.c - Simple interpreter based on 16-bit opcodes.
// John Maloney, October, 2013

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>

#include "mem.h"
#include "interp.h"

// Timer

struct timeval startTime;

void resetTimer(void) { gettimeofday(&startTime, 0); }

int msecs(void) {
	struct timeval now;
	gettimeofday(&now, 0);
	int secs = now.tv_sec - startTime.tv_sec;
	int usecs = now.tv_usec - startTime.tv_usec;
	return (secs * 1000) + (usecs / 1000);
}

// Opcodes

#define halt 0
#define pushConst 1 // true, false, nil, and small ints
#define pushLiteral 2 // literal object from literals array
#define pushVar 3
#define popVar 4
#define jmp 5
#define jmpTrue 6
#define jmpFalse 7
#define decrementAndJmp 8
#define callCmd 9
#define callReporter 10
#define changeVarBy 11

#define OP(opcode, arg) ((opcode << 24) | (arg & 0xFFFFFF))
#define CMD(n) ((n >> 24) & 0xF)
#define ARG(n) (((n & 0xFFFFFF) << 8) >> 8)

// Prims

typedef OBJ (*PrimFunc)(OBJ args[]);

void printObj(OBJ obj) {
	if (isInt(obj)) printf("%d", obj2int(obj));
	else if (obj == nilObj) printf("nil");
	else if (obj == trueObj) printf("true");
	else if (obj == falseObj) printf("false");
	else if (objClass(obj) == StringClass) {
		printf("%s", obj2str(obj));
	} else {
		printf("OBJ(addr: %d, class: %d)", (int) obj, objClass(obj));
	}
}

void printlnObj(OBJ obj) {
	printObj(obj);
	printf("\n");
}

void prim2Hello(OBJ args[]) {
	printf("hello(%d, %d)\n", (int) args[0], (int) args[1]);
}

void prim2print(OBJ args[]) { printlnObj(args[0]); }
void prim2noop(OBJ args[]) { }

OBJ prim2Add(OBJ args[]) { return int2obj(evalInt(args[0]) + evalInt(args[1])); }
OBJ prim2Sub(OBJ args[]) { return int2obj(evalInt(args[0]) - evalInt(args[1])); }
OBJ prim2Mul(OBJ args[]) { return int2obj(evalInt(args[0]) * evalInt(args[1])); }
OBJ prim2Less(OBJ args[]) { return (evalInt(args[0]) < evalInt(args[1])) ? trueObj : falseObj; }

// Array Primitives

void primFailed(char *reason) {
	// Print a message and stop the interpreter.
	printf("Primitive failed: %s\n", reason);
	exit(-1);
}

OBJ sizeFailure() { primFailed("Size must be a positive integer"); return nilObj; }
OBJ arrayClassFailure() { primFailed("Must must be an Array"); return nilObj; }
OBJ indexClassFailure() { primFailed("Index must be an integer"); return nilObj; }
OBJ outOfRangeFailure() { primFailed("Index out of range"); return nilObj; }

OBJ prim2newArray(OBJ args[]) {
	OBJ n = args[0];
	if (!isInt(n) || ((int) n < 0)) return sizeFailure();
	return newObj(ArrayClass, obj2int(n), nilObj);
}

OBJ prim2arrayAt(OBJ args[]) {
	OBJ array = args[0];
	if (NOT_CLASS(array, ArrayClass)) return arrayClassFailure();
	OBJ index = args[1];
	if (!isInt(index)) return indexClassFailure();
	
	int i = obj2int(index);
	if (NOT_CLASS(array, ArrayClass)) return arrayClassFailure();
	if ((i < 1) || (i > objWords(array))) { outOfRangeFailure(); return nilObj; }
	return (OBJ) array[HEADER_WORDS + i - 1];
}

OBJ prim2arrayAtPut(OBJ args[]) {
	OBJ array = args[0];
	if (NOT_CLASS(array, ArrayClass)) return arrayClassFailure();
	OBJ index = args[1];
	if (!isInt(index)) return indexClassFailure();
	OBJ value = args[2];
	
	int i = obj2int(index);
	if ((i < 1) || (i > objWords(array))) return outOfRangeFailure();
	array[HEADER_WORDS + i - 1] = (int) value;
	return nilObj;
}

OBJ prim2arrayAtAllPut(OBJ args[]) {
	OBJ array = args[0];
	if (NOT_CLASS(array, ArrayClass)) return arrayClassFailure();
	OBJ value = args[1];
	
	int end = objWords(array) + HEADER_WORDS;
	for (int i = HEADER_WORDS; i < end; i++) array[i] = (int) value;
	return nilObj;
}

// Interpreter State

int stack[10000];
int vars[100];
int literals[100];

// Program

int prog1[] = {
	OP(callCmd, 0),
	(int) prim2Hello,
	OP(jmp, -3),
	OP(halt, 0),
};

int prog2[] = {
	OP(pushLiteral, 0), // loop counter
	OP(callCmd, 0),
	(int) prim2Hello,
	OP(decrementAndJmp, -3),
	OP(halt, 0),
};

int prog4[] = {
	OP(pushConst, 1),
	OP(pushConst, 2),
	OP(callCmd, 2),
	(int) prim2Hello,	
	OP(halt, 0),
};

int emptyLoop[] = { // empty loop
	OP(pushLiteral, 0), // push loop counter
	OP(decrementAndJmp, -1),
	OP(halt, 0),
};

int loopTest[] = {
	OP(pushConst, (int) int2obj(0)),
	OP(popVar, 0),

	OP(pushLiteral, 0), // push loop counter
	OP(pushConst, (int) int2obj(1)),
	OP(changeVarBy, 0),
	OP(decrementAndJmp, -3),

	OP(pushVar, 0),
	OP(callCmd, 1),
	(int) prim2print,

	OP(halt, 0),
};

int sumTestWithRepeat[] = {
	OP(pushConst, (int) int2obj(0)),
	OP(popVar, 0),
	
	OP(pushLiteral, 4), // push loop counter
	OP(pushVar, 0),
	OP(pushConst, (int) int2obj(1)),
	OP(callReporter, 2),
	(int) prim2Add,
	OP(popVar, 0),
	
	OP(decrementAndJmp, -6),
	
	OP(pushVar, 0),
	OP(callCmd, 1),
	(int) prim2print,
	
	OP(halt, 0),
};

int sumTest[] = {
	OP(pushConst, (int) int2obj(0)),
	OP(popVar, 0), // sum
	OP(pushConst, (int) int2obj(0)),
	OP(popVar, 1), // i

	OP(jmp, 10), // jump to while test

	OP(pushVar, 0),
	OP(pushConst, (int) int2obj(1)), // OP(pushVar, 1),
	OP(callReporter, 2),
	(int) prim2Add,
	OP(popVar, 0),

//	OP(pushVar, 0),
//	OP(callCmd, 1),
//	(int) prim2print,

	OP(pushVar, 1),
	OP(pushConst, (int) int2obj(1)),
	OP(callReporter, 2),
	(int) prim2Add,
	OP(popVar, 1),
	
	OP(pushVar, 1),
	OP(pushLiteral, 1),
	OP(callReporter, 2),
	(int) prim2Less,
	OP(jmpTrue, -15),
	
	OP(pushVar, 0),
	OP(callCmd, 1),
	(int) prim2print,
	
	OP(halt, 0),
};

// vars and literal indices for findPrimes

#define primeCount 0
#define flags 1
#define i 2
#define j 3
#define literal8190 2
#define literal8188 3

int findPrimes[] = {
	OP(pushConst, (int) int2obj(0)),
	OP(popVar, primeCount),

	OP(pushLiteral, literal8190),
	OP(callReporter, 1),
	(int) prim2newArray,
	OP(popVar, flags),

	OP(pushVar, flags),
	OP(pushConst, (int) trueObj),
	OP(callReporter, 2),
	(int) prim2arrayAtAllPut,
	
	OP(pushConst, (int) int2obj(2)),
	OP(popVar, i),

	OP(pushLiteral, literal8188), // push loop counter
	OP(pushVar, flags),  // repeatLoopStart
	OP(pushVar, i),
	OP(callReporter, 2),
	(int) prim2arrayAt,
	OP(jmpFalse, 23), // jmpFalse ifEnd

//	OP(pushVar, i),
//	OP(callCmd, 1),
//	(int) prim2print,

	OP(pushConst, (int) int2obj(1)),
	OP(changeVarBy, primeCount),

	OP(pushConst, (int) int2obj(2)),
	OP(pushVar, i),
	OP(callReporter, 2),
	(int) prim2Mul,
	OP(popVar, j),

	OP(jmp, 7), // jmp whileEndTest
	OP(pushVar, flags), // whileLoopStart
	OP(pushVar, j),
	OP(pushConst, (int) falseObj),
	OP(callCmd, 3),
	(int) prim2arrayAtPut,

	OP(pushVar, i),
	OP(changeVarBy, j),

	OP(pushVar, j), // whileEndTest
	OP(pushLiteral, literal8190),
	OP(callReporter, 2),
	(int) prim2Less,
	OP(jmpTrue, -12), // jmpTrue whileLoopStart

	OP(pushConst, (int) int2obj(1)),  // ifEnd
	OP(changeVarBy, i),

	OP(decrementAndJmp, -31), // decrementAndJmp, repeatLoopStart

//	OP(pushVar, primeCount),
//	OP(callCmd, 1),
//	(int) prim2print,

	OP(halt, 0),
};

int primes1000[] = {
	OP(pushLiteral, literal8190),
	OP(callReporter, 1),
	(int) prim2newArray,
	OP(popVar, flags),
	
	OP(pushConst, 1000), // outer loop counter

	OP(pushConst, (int) int2obj(0)),
	OP(popVar, primeCount),
	
	OP(pushVar, flags),
	OP(pushConst, (int) trueObj),
	OP(callCmd, 2),
	(int) prim2arrayAtAllPut,
	
	OP(pushConst, (int) int2obj(2)),
	OP(popVar, i),
	
	OP(pushLiteral, literal8188), // push loop counter
	OP(pushVar, flags),  // repeatLoopStart
	OP(pushVar, i),
	OP(callReporter, 2),
	(int) prim2arrayAt,
	OP(jmpFalse, 20), // jmpFalse ifEnd
	
	OP(pushConst, (int) int2obj(1)),
	OP(changeVarBy, primeCount),
	
	OP(pushConst, (int) int2obj(2)),
	OP(pushVar, i),
	OP(callReporter, 2),
	(int) prim2Mul,
	OP(popVar, j),
	
	OP(jmp, 7), // jmp whileEndTest
	OP(pushVar, flags), // whileLoopStart
	OP(pushVar, j),
	OP(pushConst, (int) falseObj),
	OP(callCmd, 3),
	(int) prim2arrayAtPut,
	
	OP(pushVar, i),
	OP(changeVarBy, j),
	
	OP(pushVar, j), // whileEndTest
	OP(pushLiteral, literal8190),
	OP(callReporter, 2),
	(int) prim2Less,
	OP(jmpTrue, -12), // jmpTrue whileLoopStart
	
	OP(pushConst, (int) int2obj(1)),  // ifEnd
	OP(changeVarBy, i),
	
	OP(decrementAndJmp, -28), // decrementAndJmp, repeatLoopStart

//	OP(pushVar, primeCount),
//	OP(callCmd, 1),
//	(int) prim2print,
	
	OP(decrementAndJmp, -38), // decrementAndJmp, outerRepeatLoopStart
	
	OP(halt, 0),
};

void initLiterals() {
	literals[0] = 100000000;  // loop counter, not int obj
	literals[1] = (int) int2obj(10000000);
	literals[2] = (int) int2obj(8190);
	literals[3] = 8188; // loop counter, not int obj
	literals[4] = 10000000;  // loop counter, not int obj
}

void showStack(int *sp, int *fp) {
	int *ptr = sp;
	printf("sp:	%d\n", *ptr);
	while (--ptr >= &stack[0]) {
		printf("%s	%d\n", ((fp == ptr) ? "fp:" : ""), *ptr);
	}
	printf("-----\n");
}

void runProg(int *prog) {
	int *sp = stack;
	int *ip = prog;
	int nargs;
	PrimFunc prim;

	initLiterals();
	while (true) {
		int op = *ip++;
		// printf("ip: %d cmd: %d sp: %d\n", (ip - prog), CMD(op), (sp - stack));
		switch (CMD(op)) {
			case halt:
				return;
			case pushConst:
				*sp++ = ARG(op);
				break;
			case pushLiteral:
				*sp++ = literals[ARG(op)];
				break;
			case pushVar:
				*sp++ = vars[ARG(op)];
				break;
			case popVar:
				vars[ARG(op)] = *--sp;
				break;
			case changeVarBy:
				vars[ARG(op)] = (int) int2obj(evalInt((OBJ) vars[ARG(op)]) + evalInt((OBJ) (*--sp)));
				break;
			case jmp:
				ip += ARG(op);
				break;
			case jmpTrue:
				if ((int) trueObj == (*--sp)) ip += ARG(op);
				break;
			case jmpFalse:
				if ((int) falseObj == (*--sp)) ip += ARG(op);
				break;
			case decrementAndJmp:
				if ((--*(sp - 1)) > 0) ip += ARG(op); // loop counter > 0, so branch
				else sp--; // pop loop count
				break;
			case callCmd:
				nargs = ARG(op);
				prim = (PrimFunc) *ip++;
				prim((OBJ *) (sp - nargs));
				sp -= nargs;
				break;
			case callReporter:
				nargs = ARG(op);
				prim = (PrimFunc) *ip++;
				*(sp - nargs) = (int) prim((OBJ *) sp - nargs);
				sp -= nargs - 1;
				break;
			default:
				printf("Unknown opcode: %d\n", ((op >> 24) & 0xF));
				return;
		}
	}
}


int main(int argc, char *argv[]) {
	memInit(100000);

	resetTimer();
	runProg(emptyLoop);
	printf("empty loop: %d msecs\n", msecs());

	resetTimer();
	runProg(loopTest);
	printf("loopTest: %d msecs\n", msecs());

	resetTimer();
	runProg(sumTest);
	printf("sumTest: %d msecs\n", msecs());

	resetTimer();
	runProg(sumTestWithRepeat);
	printf("sumTestWithRepeat: %d msecs\n", msecs());

	resetTimer();
	runProg(primes1000);
	printf("find primes 1000: %d msecs\n", msecs());
	
}
