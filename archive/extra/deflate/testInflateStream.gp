call (function {
  d = (array 137 80 78 71 13 10 26 10 0 0 0 13 73 72 68 82 0 0 0 16 0 0 0 8 8 6 0 0 0 240 118 127 151 0 0 1 18 73 68 65 84 120 94 61 81 219 118 132 32 12 228 255 127 171 79 125 104 187 85 169 139 10 10 42 138 151 221 79 152 50 177 91 207 201 9 146 153 201 36 40 126 243 108 48 12 21 198 209 192 251 26 109 251 133 105 186 99 158 91 104 253 14 107 53 226 216 192 185 74 106 196 185 190 70 223 107 168 16 126 176 44 237 159 64 157 207 22 93 119 203 224 34 231 34 11 53 136 145 209 201 153 247 33 220 97 204 71 22 46 161 88 36 153 221 88 96 16 204 226 227 17 179 96 15 54 161 248 85 191 240 197 237 45 99 190 161 166 160 255 5 216 225 149 41 66 39 36 174 171 19 251 20 242 94 75 140 190 18 55 170 105 62 5 124 28 65 200 84 77 201 99 219 6 249 103 141 157 57 239 228 153 75 144 195 93 241 94 81 141 14 56 59 59 167 212 75 225 249 92 228 204 238 36 189 92 210 17 201 220 1 23 170 216 145 68 118 72 209 34 114 251 25 52 7 35 228 109 115 226 230 194 148 226 144 153 206 100 132 235 21 172 108 122 200 23 231 238 176 70 131 148 95 230 60 71 240 137 185 200 125 207 99 29 94 92 49 248 204 116 243 11 173 192 202 131 9 82 101 143 0 0 0 0 73 69 78 68 174 66 96 130)
  b = (toBinaryData d)

  data = (dataStream b true)

  (nextUInt8 data)
  (nextUInt8 data)
  (nextUInt8 data)
  (nextUInt8 data)

  (nextUInt8 data)
  (nextUInt8 data)
  (nextUInt8 data)
  (nextUInt8 data)

  length = (nextUInt32 data)
  assert length 13 'first chunk'

  chunkType = (nextString data 4)
  assert chunkType 'IHDR' 'header'

  width = (nextUInt32 data)
  assert width 16 'width'
  height = (nextUInt32 data)
  assert height 8 'height'

  bitsPerChannel = (nextUInt8 data)
  assert bitsPerChannel 8
  colorType = (nextUInt8 data)
  assert colorType 6 'direct color'

  compression = (nextUInt8 data)
  filterMethod = (nextUInt8 data)
  interlaceMethod = (nextUInt8 data)

  crc = (nextUInt32 data)

  length = (nextUInt32 data)
  assert length 274 'second chunk'
  chunkType = (nextString data 4)
  assert chunkType 'IDAT' 'data'

  chunk = (nextData data length)
  crc = (nextUInt32 data)

  length = (nextUInt32 data)
  assert length 0 'third and end chunk'
  chunkType = (nextString data 4)
  assert chunkType 'IEND' 'data'

  s = (onZLibData (new 'InflateStream') chunk)
  result = (nextAll s)
  assert (byteCount result) 520
  return s
})
