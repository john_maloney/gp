d = (new 'D1Recognizer')
initTemplates d

for template (templateData d) {
  gc
  n = (at template 1)
  input = (makePointsFromData d (at template 2))
  s = (new 'D1Stroke')
  preProcess s input
  result = (recognize d input)
  assert (at result 1) n 'recognize'
  assert ((at result 2) > (99.0 / 100)) true 'recognize score'
}
