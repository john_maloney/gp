defineClass OMetaParser temps cursors currentList matcherError memo
method match OMetaParser input rule {
  cursors = (list 1)
  matcherError = (array 'matcherror')
  currentList = input
  return (call rule this)
}
method apply OMetaParser rule {
  shouldMemo = false
  if (notNil memo) {
    if ((argCount) == 2) {
      if ((count cursors) == 1) {
        pos = (at cursors 1)
        m = (at memo pos)
        if (notNil m) {
          v = (at m rule)
          if (notNil v) {
            atPut cursors 1 (at v 2)
            return (at v 1)
          }
        }
        shouldMemo = true
      }
    }
  }
  if ((argCount) == 2) {
    result = (call rule this)
  } else {
    args = (newArray ((argCount) - 1))
    atPut args 1 this
    for i ((argCount) - 2) {
      atPut args (i + 1) (arg (i + 2))
    }
    result = (callWith rule args)
  }
  if (not shouldMemo) {
    return result
  }
  newPos = (at cursors 1)
  if (isNil m) {
    m = (dictionary)
    atPut memo pos m
  }
  atPut m rule (array result newPos)
  return result
}
method exactly OMetaParser str {
  if ((at cursors 1) <= (count currentList)) {
    if (str == (at currentList (at cursors 1))) {
      result = (at currentList (at cursors 1))
      atPut cursors 1 (+ (at cursors 1) 1)
      return result
    }
  }
  return matcherError
}
method seq OMetaParser str {
  s = (letters str)
  origCursor = (at cursors 1)
  for i (count s) {
    if ((at cursors 1) <= (count currentList)) {
      if ((at s i) == (at currentList (at cursors 1))) {
        atPut cursors 1 (+ (at cursors 1) 1)
      } else {
        atPut cursors 1 origCursor
        return matcherError
      }
    } else {
      atPut cursors 1 origCursor
      return matcherError
    }
  }
  return str
}
method end OMetaParser {
  if ((at cursors 1) > (count currentList)) {
    return true
  }
  return matcherError
}
method token OMetaParser str {
  origCursor = (at cursors 1)
  spaces this
  result = (seq this str)
  if (result === matcherError) {
    atPut cursors 1 origCursor
  }
  return result
}
method spaces OMetaParser {
  while true {
    if ((at cursors 1) <= (count currentList)) {
       if (isWhiteSpace (at currentList (at cursors 1))) {
         atPut cursors 1 (+ (at cursors 1) 1)
       } else {
         return nil
       }
    } else {
       return nil
    }
  }
  return nil
}
method anything OMetaParser {
  if ((at cursors 1) <= (count currentList)) {
    result = (at currentList (at cursors 1))
    atPut cursors 1 (+ (at cursors 1) 1)
    return result
  }
  return matcherError
}
method empty OMetaParser {
  return true
}
method useMemo OMetaParser {
  memo = (dictionary)
}

method args OMetaParser {
origCursor1 = (at cursors 1)
done1 = false
result1 = nil
if (not done1) {
origCursor2 = (at cursors 1)
result2 = nil
if (not (result2 === matcherError)) {
result3 = (apply this 'seq' '(')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
if (not (result2 === matcherError)) {
origCursor3 = (at cursors 1)
result4 = (apply this 'gpExpressionString')
result3 = result4
if (result3 === matcherError) {
atPut cursors 1 origCursor3
} else {
g1 = result3
}
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
if (not (result2 === matcherError)) {
origCursor3 = (at cursors 1)
result4 = (list)
val4 = nil
while (not (val4 === matcherError)) {
origCursor5 = (at cursors 1)
result5 = nil
if (not (result5 === matcherError)) {
result6 = (apply this 'spaces')
result5 = result6
if (result5 === matcherError) {
atPut cursors 1 origCursor5
}
}
if (not (result5 === matcherError)) {
result6 = (apply this 'seq' ',')
result5 = result6
if (result5 === matcherError) {
atPut cursors 1 origCursor5
}
}
if (not (result5 === matcherError)) {
result6 = (apply this 'gpExpressionString')
result5 = result6
if (result5 === matcherError) {
atPut cursors 1 origCursor5
}
}
val4 = result5
if (not (val4 === matcherError)) {
add result4 val4
}
}
result3 = result4
if (result3 === matcherError) {
atPut cursors 1 origCursor3
} else {
gs = result3
}
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
if (not (result2 === matcherError)) {
result3 = (apply this 'token' ')')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
if (not (result2 === matcherError)) {
result3 = (join (array g1) gs)
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
result1 = result2
if (not (result1 === matcherError)) {
done1 = true
} else {
atPut cursors 1 origCursor1
}
}
if (not done1) {
origCursor2 = (at cursors 1)
result2 = nil
if (not (result2 === matcherError)) {
origCursor3 = (at cursors 1)
result4 = (apply this 'seq' '(')
result3 = result4
atPut cursors 1 origCursor3
if (result3 === matcherError) {
result3 = true
} else {
result3 = matcherError
}
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
if (not (result2 === matcherError)) {
result3 = (array)
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
result1 = result2
if (not (result1 === matcherError)) {
done1 = true
} else {
atPut cursors 1 origCursor1
}
}
if (not done1) {
result1 = matcherError
}
result1 = result2
return result1
}

method letter OMetaParser {
origCursor1 = (at cursors 1)
result1 = nil
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (apply this 'anything')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
x = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (isLetter x)
if (not result2) {
result2 = matcherError
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = x
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
result1 = result2
return result1
}

method digit OMetaParser {
origCursor1 = (at cursors 1)
result1 = nil
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (apply this 'anything')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
x = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (isDigit x)
if (not result2) {
result2 = matcherError
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = x
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
result1 = result2
return result1
}

method name OMetaParser {
origCursor1 = (at cursors 1)
result1 = nil
if (not (result1 === matcherError)) {
result2 = (apply this 'spaces')
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
origCursor3 = (at cursors 1)
origCursor4 = (at cursors 1)
result4 = nil
if (not (result4 === matcherError)) {
result5 = (apply this 'letter')
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
}
}
if (not (result4 === matcherError)) {
result5 = (list)
val5 = nil
while (not (val5 === matcherError)) {
origCursor6 = (at cursors 1)
done6 = false
result6 = nil
if (not done6) {
origCursor7 = (at cursors 1)
result7 = nil
if (not (result7 === matcherError)) {
result8 = (apply this 'letter')
result7 = result8
if (result7 === matcherError) {
atPut cursors 1 origCursor7
}
}
result6 = result7
if (not (result6 === matcherError)) {
done6 = true
} else {
atPut cursors 1 origCursor6
}
}
if (not done6) {
origCursor7 = (at cursors 1)
result7 = nil
if (not (result7 === matcherError)) {
result8 = (apply this 'digit')
result7 = result8
if (result7 === matcherError) {
atPut cursors 1 origCursor7
}
}
result6 = result7
if (not (result6 === matcherError)) {
done6 = true
} else {
atPut cursors 1 origCursor6
}
}
if (not done6) {
result6 = matcherError
}
val5 = result6
if (not (val5 === matcherError)) {
add result5 val5
}
}
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
}
}
result3 = result4
if (result3 === matcherError) {
at cursors 1 origCursor3
} else {
result3 = (toArray (copyList currentList ((at cursors 1) - origCursor3) origCursor3))
}
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
cs = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (joinStringArray cs)
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
result1 = result2
return result1
}

method optIter OMetaParser {
origCursor1 = (at cursors 1)
done1 = false
result1 = nil
if (not done1) {
origCursor2 = (at cursors 1)
result2 = nil
if (not (result2 === matcherError)) {
result3 = (apply this 'token' '*')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
if (not (result2 === matcherError)) {
result3 = 'many0'
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
result1 = result2
if (not (result1 === matcherError)) {
done1 = true
} else {
atPut cursors 1 origCursor1
}
}
if (not done1) {
origCursor2 = (at cursors 1)
result2 = nil
if (not (result2 === matcherError)) {
result3 = (apply this 'token' '+')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
if (not (result2 === matcherError)) {
result3 = 'many1'
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
result1 = result2
if (not (result1 === matcherError)) {
done1 = true
} else {
atPut cursors 1 origCursor1
}
}
if (not done1) {
origCursor2 = (at cursors 1)
result2 = nil
if (not (result2 === matcherError)) {
result3 = (apply this 'token' '?')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
if (not (result2 === matcherError)) {
origCursor3 = (at cursors 1)
result4 = (apply this 'seq' '{')
result3 = result4
atPut cursors 1 origCursor3
if (result3 === matcherError) {
result3 = true
} else {
result3 = matcherError
}
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
if (not (result2 === matcherError)) {
result3 = 'option'
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
result1 = result2
if (not (result1 === matcherError)) {
done1 = true
} else {
atPut cursors 1 origCursor1
}
}
if (not done1) {
result1 = matcherError
}
result1 = result2
return result1
}

method application OMetaParser {
origCursor1 = (at cursors 1)
result1 = nil
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (apply this 'name')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
r = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (apply this 'args')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
as = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (join (array 'app' r) as)
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
result1 = result2
return result1
}

method semanticAction OMetaParser {
origCursor1 = (at cursors 1)
result1 = nil
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (apply this 'token' '->')
result2 = result3
if (result2 === matcherError) {
at cursors 1 origCursor2
result2 = nil
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (apply this 'token' '{')
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (apply this 'gpExpressionString')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
e = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (apply this 'token' '}')
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (array 'action' e)
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
result1 = result2
return result1
}

method semanticPredicate OMetaParser {
origCursor1 = (at cursors 1)
result1 = nil
if (not (result1 === matcherError)) {
result2 = (apply this 'token' '?{')
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (apply this 'gpExpressionString')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
e = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (apply this 'token' '}')
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (array 'predAction' e)
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
result1 = result2
return result1
}

method stringLiteral OMetaParser {
origCursor1 = (at cursors 1)
result1 = nil
if (not (result1 === matcherError)) {
result2 = (apply this 'token' '''')
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (list)
val3 = nil
while (not (val3 === matcherError)) {
origCursor4 = (at cursors 1)
done4 = false
result4 = nil
if (not done4) {
origCursor5 = (at cursors 1)
result5 = nil
if (not (result5 === matcherError)) {
origCursor6 = (at cursors 1)
result6 = nil
if (not (result6 === matcherError)) {
origCursor7 = (at cursors 1)
origCursor8 = (at cursors 1)
result8 = nil
if (not (result8 === matcherError)) {
result9 = (apply this 'seq' '''')
result8 = result9
if (result8 === matcherError) {
atPut cursors 1 origCursor8
}
}
if (not (result8 === matcherError)) {
result9 = (apply this 'seq' '''')
result8 = result9
if (result8 === matcherError) {
atPut cursors 1 origCursor8
}
}
result7 = result8
atPut cursors 1 origCursor7
result6 = result7
if (result6 === matcherError) {
atPut cursors 1 origCursor6
}
}
if (not (result6 === matcherError)) {
result7 = (apply this 'anything')
result6 = result7
if (result6 === matcherError) {
atPut cursors 1 origCursor6
}
}
if (not (result6 === matcherError)) {
result7 = (apply this 'anything')
result6 = result7
if (result6 === matcherError) {
atPut cursors 1 origCursor6
}
}
result5 = result6
if (result5 === matcherError) {
atPut cursors 1 origCursor5
}
}
result4 = result5
if (not (result4 === matcherError)) {
done4 = true
} else {
atPut cursors 1 origCursor4
}
}
if (not done4) {
origCursor5 = (at cursors 1)
result5 = nil
if (not (result5 === matcherError)) {
origCursor6 = (at cursors 1)
result7 = (apply this 'seq' '''')
result6 = result7
atPut cursors 1 origCursor6
if (result6 === matcherError) {
result6 = true
} else {
result6 = matcherError
}
result5 = result6
if (result5 === matcherError) {
atPut cursors 1 origCursor5
}
}
if (not (result5 === matcherError)) {
result6 = (apply this 'anything')
result5 = result6
if (result5 === matcherError) {
atPut cursors 1 origCursor5
}
}
result4 = result5
if (not (result4 === matcherError)) {
done4 = true
} else {
atPut cursors 1 origCursor4
}
}
if (not done4) {
result4 = matcherError
}
val3 = result4
if (not (val3 === matcherError)) {
add result3 val3
}
}
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
xs = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (apply this 'seq' '''')
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (array 'app' 'seq' (joinStringArray (toArray xs)))
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
result1 = result2
return result1
}

method stringAtom OMetaParser {
origCursor1 = (at cursors 1)
result1 = nil
if (not (result1 === matcherError)) {
result2 = (apply this 'token' '#''')
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (list)
val3 = nil
while (not (val3 === matcherError)) {
origCursor4 = (at cursors 1)
done4 = false
result4 = nil
if (not done4) {
origCursor5 = (at cursors 1)
result5 = nil
if (not (result5 === matcherError)) {
origCursor6 = (at cursors 1)
result6 = nil
if (not (result6 === matcherError)) {
origCursor7 = (at cursors 1)
origCursor8 = (at cursors 1)
result8 = nil
if (not (result8 === matcherError)) {
result9 = (apply this 'seq' '''')
result8 = result9
if (result8 === matcherError) {
atPut cursors 1 origCursor8
}
}
if (not (result8 === matcherError)) {
result9 = (apply this 'seq' '''')
result8 = result9
if (result8 === matcherError) {
atPut cursors 1 origCursor8
}
}
result7 = result8
atPut cursors 1 origCursor7
result6 = result7
if (result6 === matcherError) {
atPut cursors 1 origCursor6
}
}
if (not (result6 === matcherError)) {
result7 = (apply this 'anything')
result6 = result7
if (result6 === matcherError) {
atPut cursors 1 origCursor6
}
}
if (not (result6 === matcherError)) {
result7 = (apply this 'anything')
result6 = result7
if (result6 === matcherError) {
atPut cursors 1 origCursor6
}
}
result5 = result6
if (result5 === matcherError) {
atPut cursors 1 origCursor5
}
}
result4 = result5
if (not (result4 === matcherError)) {
done4 = true
} else {
atPut cursors 1 origCursor4
}
}
if (not done4) {
origCursor5 = (at cursors 1)
result5 = nil
if (not (result5 === matcherError)) {
origCursor6 = (at cursors 1)
result7 = (apply this 'seq' '''')
result6 = result7
atPut cursors 1 origCursor6
if (result6 === matcherError) {
result6 = true
} else {
result6 = matcherError
}
result5 = result6
if (result5 === matcherError) {
atPut cursors 1 origCursor5
}
}
if (not (result5 === matcherError)) {
result6 = (apply this 'anything')
result5 = result6
if (result5 === matcherError) {
atPut cursors 1 origCursor5
}
}
result4 = result5
if (not (result4 === matcherError)) {
done4 = true
} else {
atPut cursors 1 origCursor4
}
}
if (not done4) {
result4 = matcherError
}
val3 = result4
if (not (val3 === matcherError)) {
add result3 val3
}
}
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
xs = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (apply this 'seq' '''')
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (array 'app' 'exactly' (joinStringArray (toArray xs)))
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
result1 = result2
return result1
}

method tokenSugar OMetaParser {
origCursor1 = (at cursors 1)
result1 = nil
if (not (result1 === matcherError)) {
result2 = (apply this 'spaces')
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (apply this 'seq' '"')
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
origCursor3 = (at cursors 1)
origCursor4 = (at cursors 1)
result4 = nil
if (not (result4 === matcherError)) {
result5 = (list)
val5 = nil
while (not (val5 === matcherError)) {
origCursor6 = (at cursors 1)
result6 = nil
if (not (result6 === matcherError)) {
origCursor7 = (at cursors 1)
result8 = (apply this 'seq' '"')
result7 = result8
atPut cursors 1 origCursor7
if (result7 === matcherError) {
result7 = true
} else {
result7 = matcherError
}
result6 = result7
if (result6 === matcherError) {
atPut cursors 1 origCursor6
}
}
if (not (result6 === matcherError)) {
result7 = (apply this 'anything')
result6 = result7
if (result6 === matcherError) {
atPut cursors 1 origCursor6
}
}
val5 = result6
if (not (val5 === matcherError)) {
add result5 val5
}
}
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
}
}
result3 = result4
if (result3 === matcherError) {
at cursors 1 origCursor3
} else {
result3 = (toArray (copyList currentList ((at cursors 1) - origCursor3) origCursor3))
}
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
xs = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (apply this 'seq' '"')
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (array 'app' 'token' (joinStringArray (toArray xs)))
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
result1 = result2
return result1
}

method expr OMetaParser {
origCursor1 = (at cursors 1)
done1 = false
result1 = nil
if (not done1) {
origCursor2 = (at cursors 1)
result2 = nil
if (not (result2 === matcherError)) {
origCursor3 = (at cursors 1)
result4 = (apply this 'expr4')
result3 = result4
if (result3 === matcherError) {
atPut cursors 1 origCursor3
} else {
e1 = result3
}
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
if (not (result2 === matcherError)) {
origCursor3 = (at cursors 1)
origCursor4 = (at cursors 1)
result4 = (list)
val4 = nil
first4 = true
while (not (val4 === matcherError)) {
origCursor5 = (at cursors 1)
result5 = nil
if (not (result5 === matcherError)) {
result6 = (apply this 'spaces')
result5 = result6
if (result5 === matcherError) {
atPut cursors 1 origCursor5
}
}
if (not (result5 === matcherError)) {
result6 = (apply this 'seq' '|')
result5 = result6
if (result5 === matcherError) {
atPut cursors 1 origCursor5
}
}
if (not (result5 === matcherError)) {
result6 = (apply this 'expr4')
result5 = result6
if (result5 === matcherError) {
atPut cursors 1 origCursor5
}
}
val4 = result5
if (not (val4 === matcherError)) {
add result4 val4
} else {
if first4 {
atPut cursors 1 origCursor4
result4 = matcherError
}
}
first4 = false
}
result3 = result4
if (result3 === matcherError) {
atPut cursors 1 origCursor3
} else {
es = result3
}
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
if (not (result2 === matcherError)) {
result3 = (join (array 'or' e1) es)
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
result1 = result2
if (not (result1 === matcherError)) {
done1 = true
} else {
atPut cursors 1 origCursor1
}
}
if (not done1) {
origCursor2 = (at cursors 1)
result2 = nil
if (not (result2 === matcherError)) {
result3 = (apply this 'expr4')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
result1 = result2
if (not (result1 === matcherError)) {
done1 = true
} else {
atPut cursors 1 origCursor1
}
}
if (not done1) {
result1 = matcherError
}
result1 = result2
return result1
}

method expr1 OMetaParser {
origCursor1 = (at cursors 1)
done1 = false
result1 = nil
if (not done1) {
origCursor2 = (at cursors 1)
result2 = nil
if (not (result2 === matcherError)) {
result3 = (apply this 'application')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
result1 = result2
if (not (result1 === matcherError)) {
done1 = true
} else {
atPut cursors 1 origCursor1
}
}
if (not done1) {
origCursor2 = (at cursors 1)
result2 = nil
if (not (result2 === matcherError)) {
result3 = (apply this 'semanticAction')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
result1 = result2
if (not (result1 === matcherError)) {
done1 = true
} else {
atPut cursors 1 origCursor1
}
}
if (not done1) {
origCursor2 = (at cursors 1)
result2 = nil
if (not (result2 === matcherError)) {
result3 = (apply this 'semanticPredicate')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
result1 = result2
if (not (result1 === matcherError)) {
done1 = true
} else {
atPut cursors 1 origCursor1
}
}
if (not done1) {
origCursor2 = (at cursors 1)
result2 = nil
if (not (result2 === matcherError)) {
result3 = (apply this 'tokenSugar')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
result1 = result2
if (not (result1 === matcherError)) {
done1 = true
} else {
atPut cursors 1 origCursor1
}
}
if (not done1) {
origCursor2 = (at cursors 1)
result2 = nil
if (not (result2 === matcherError)) {
result3 = (apply this 'stringAtom')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
result1 = result2
if (not (result1 === matcherError)) {
done1 = true
} else {
atPut cursors 1 origCursor1
}
}
if (not done1) {
origCursor2 = (at cursors 1)
result2 = nil
if (not (result2 === matcherError)) {
result3 = (apply this 'stringLiteral')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
result1 = result2
if (not (result1 === matcherError)) {
done1 = true
} else {
atPut cursors 1 origCursor1
}
}
if (not done1) {
origCursor2 = (at cursors 1)
result2 = nil
if (not (result2 === matcherError)) {
result3 = (apply this 'token' '[')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
if (not (result2 === matcherError)) {
origCursor3 = (at cursors 1)
result4 = (apply this 'expr')
result3 = result4
if (result3 === matcherError) {
atPut cursors 1 origCursor3
} else {
e = result3
}
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
if (not (result2 === matcherError)) {
result3 = (apply this 'token' ']')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
if (not (result2 === matcherError)) {
result3 = (array 'form' e)
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
result1 = result2
if (not (result1 === matcherError)) {
done1 = true
} else {
atPut cursors 1 origCursor1
}
}
if (not done1) {
origCursor2 = (at cursors 1)
result2 = nil
if (not (result2 === matcherError)) {
result3 = (apply this 'token' '<')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
if (not (result2 === matcherError)) {
origCursor3 = (at cursors 1)
result4 = (apply this 'expr')
result3 = result4
if (result3 === matcherError) {
atPut cursors 1 origCursor3
} else {
e = result3
}
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
if (not (result2 === matcherError)) {
result3 = (apply this 'token' '>')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
if (not (result2 === matcherError)) {
result3 = (array 'consume' e)
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
result1 = result2
if (not (result1 === matcherError)) {
done1 = true
} else {
atPut cursors 1 origCursor1
}
}
if (not done1) {
origCursor2 = (at cursors 1)
result2 = nil
if (not (result2 === matcherError)) {
result3 = (apply this 'token' '(')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
if (not (result2 === matcherError)) {
origCursor3 = (at cursors 1)
result4 = (apply this 'expr')
result3 = result4
if (result3 === matcherError) {
atPut cursors 1 origCursor3
} else {
e = result3
}
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
if (not (result2 === matcherError)) {
result3 = (apply this 'token' ')')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
if (not (result2 === matcherError)) {
result3 = e
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
result1 = result2
if (not (result1 === matcherError)) {
done1 = true
} else {
atPut cursors 1 origCursor1
}
}
if (not done1) {
result1 = matcherError
}
result1 = result2
return result1
}

method expr2 OMetaParser {
origCursor1 = (at cursors 1)
done1 = false
result1 = nil
if (not done1) {
origCursor2 = (at cursors 1)
result2 = nil
if (not (result2 === matcherError)) {
result3 = (apply this 'token' '~')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
if (not (result2 === matcherError)) {
origCursor3 = (at cursors 1)
result4 = (apply this 'expr2')
result3 = result4
if (result3 === matcherError) {
atPut cursors 1 origCursor3
} else {
x = result3
}
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
if (not (result2 === matcherError)) {
result3 = (array 'not' x)
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
result1 = result2
if (not (result1 === matcherError)) {
done1 = true
} else {
atPut cursors 1 origCursor1
}
}
if (not done1) {
origCursor2 = (at cursors 1)
result2 = nil
if (not (result2 === matcherError)) {
result3 = (apply this 'token' '&')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
if (not (result2 === matcherError)) {
origCursor3 = (at cursors 1)
result4 = (apply this 'expr2')
result3 = result4
if (result3 === matcherError) {
atPut cursors 1 origCursor3
} else {
x = result3
}
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
if (not (result2 === matcherError)) {
result3 = (array 'lookahead' x)
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
result1 = result2
if (not (result1 === matcherError)) {
done1 = true
} else {
atPut cursors 1 origCursor1
}
}
if (not done1) {
origCursor2 = (at cursors 1)
result2 = nil
if (not (result2 === matcherError)) {
result3 = (apply this 'expr1')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
}
}
result1 = result2
if (not (result1 === matcherError)) {
done1 = true
} else {
atPut cursors 1 origCursor1
}
}
if (not done1) {
result1 = matcherError
}
result1 = result2
return result1
}

method expr3 OMetaParser {
origCursor1 = (at cursors 1)
result1 = nil
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
origCursor3 = (at cursors 1)
done3 = false
result3 = nil
if (not done3) {
origCursor4 = (at cursors 1)
result4 = nil
if (not (result4 === matcherError)) {
origCursor5 = (at cursors 1)
result6 = (apply this 'expr2')
result5 = result6
if (result5 === matcherError) {
atPut cursors 1 origCursor5
} else {
x = result5
}
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
}
}
if (not (result4 === matcherError)) {
origCursor5 = (at cursors 1)
result6 = (apply this 'optIter')
result5 = result6
if (result5 === matcherError) {
atPut cursors 1 origCursor5
} else {
o = result5
}
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
}
}
if (not (result4 === matcherError)) {
result5 = (array o x)
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
}
}
result3 = result4
if (not (result3 === matcherError)) {
done3 = true
} else {
atPut cursors 1 origCursor3
}
}
if (not done3) {
origCursor4 = (at cursors 1)
result4 = nil
if (not (result4 === matcherError)) {
result5 = (apply this 'expr2')
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
}
}
result3 = result4
if (not (result3 === matcherError)) {
done3 = true
} else {
atPut cursors 1 origCursor3
}
}
if (not done3) {
result3 = matcherError
}
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
x = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
done2 = false
result2 = nil
if (not done2) {
origCursor3 = (at cursors 1)
result3 = nil
if (not (result3 === matcherError)) {
result4 = (apply this 'token' ':')
result3 = result4
if (result3 === matcherError) {
atPut cursors 1 origCursor3
}
}
if (not (result3 === matcherError)) {
origCursor4 = (at cursors 1)
result5 = (apply this 'name')
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
} else {
n = result4
}
result3 = result4
if (result3 === matcherError) {
atPut cursors 1 origCursor3
}
}
if (not (result3 === matcherError)) {
result4 = (addTemp this n)
result3 = result4
if (result3 === matcherError) {
atPut cursors 1 origCursor3
}
}
if (not (result3 === matcherError)) {
result4 = (array 'bind' n x)
result3 = result4
if (result3 === matcherError) {
atPut cursors 1 origCursor3
}
}
result2 = result3
if (not (result2 === matcherError)) {
done2 = true
} else {
atPut cursors 1 origCursor2
}
}
if (not done2) {
origCursor3 = (at cursors 1)
result3 = nil
if (not (result3 === matcherError)) {
result4 = (apply this 'empty')
result3 = result4
if (result3 === matcherError) {
atPut cursors 1 origCursor3
}
}
if (not (result3 === matcherError)) {
result4 = x
result3 = result4
if (result3 === matcherError) {
atPut cursors 1 origCursor3
}
}
result2 = result3
if (not (result2 === matcherError)) {
done2 = true
} else {
atPut cursors 1 origCursor2
}
}
if (not done2) {
result2 = matcherError
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
result1 = result2
return result1
}

method expr4 OMetaParser {
origCursor1 = (at cursors 1)
result1 = nil
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (list)
val3 = nil
while (not (val3 === matcherError)) {
result4 = (apply this 'expr3')
val3 = result4
if (not (val3 === matcherError)) {
add result3 val3
}
}
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
xs = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (join (array 'and') xs)
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
result1 = result2
return result1
}

method gpExpression OMetaParser {
origCursor1 = (at cursors 1)
result1 = nil
if (not (result1 === matcherError)) {
result2 = (list)
val2 = nil
while (not (val2 === matcherError)) {
origCursor3 = (at cursors 1)
done3 = false
result3 = nil
if (not done3) {
origCursor4 = (at cursors 1)
result4 = nil
if (not (result4 === matcherError)) {
result5 = (apply this 'spaces')
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
}
}
if (not (result4 === matcherError)) {
result5 = (apply this 'seq' '''')
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
}
}
if (not (result4 === matcherError)) {
result5 = (list)
val5 = nil
while (not (val5 === matcherError)) {
origCursor6 = (at cursors 1)
done6 = false
result6 = nil
if (not done6) {
origCursor7 = (at cursors 1)
result7 = nil
if (not (result7 === matcherError)) {
origCursor8 = (at cursors 1)
result8 = nil
if (not (result8 === matcherError)) {
origCursor9 = (at cursors 1)
origCursor10 = (at cursors 1)
result10 = nil
if (not (result10 === matcherError)) {
result11 = (apply this 'seq' '''')
result10 = result11
if (result10 === matcherError) {
atPut cursors 1 origCursor10
}
}
if (not (result10 === matcherError)) {
result11 = (apply this 'seq' '''')
result10 = result11
if (result10 === matcherError) {
atPut cursors 1 origCursor10
}
}
result9 = result10
atPut cursors 1 origCursor9
result8 = result9
if (result8 === matcherError) {
atPut cursors 1 origCursor8
}
}
if (not (result8 === matcherError)) {
result9 = (apply this 'anything')
result8 = result9
if (result8 === matcherError) {
atPut cursors 1 origCursor8
}
}
if (not (result8 === matcherError)) {
result9 = (apply this 'anything')
result8 = result9
if (result8 === matcherError) {
atPut cursors 1 origCursor8
}
}
result7 = result8
if (result7 === matcherError) {
atPut cursors 1 origCursor7
}
}
result6 = result7
if (not (result6 === matcherError)) {
done6 = true
} else {
atPut cursors 1 origCursor6
}
}
if (not done6) {
origCursor7 = (at cursors 1)
result7 = nil
if (not (result7 === matcherError)) {
origCursor8 = (at cursors 1)
result9 = (apply this 'seq' '''')
result8 = result9
atPut cursors 1 origCursor8
if (result8 === matcherError) {
result8 = true
} else {
result8 = matcherError
}
result7 = result8
if (result7 === matcherError) {
atPut cursors 1 origCursor7
}
}
if (not (result7 === matcherError)) {
result8 = (apply this 'anything')
result7 = result8
if (result7 === matcherError) {
atPut cursors 1 origCursor7
}
}
result6 = result7
if (not (result6 === matcherError)) {
done6 = true
} else {
atPut cursors 1 origCursor6
}
}
if (not done6) {
result6 = matcherError
}
val5 = result6
if (not (val5 === matcherError)) {
add result5 val5
}
}
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
}
}
if (not (result4 === matcherError)) {
result5 = (apply this 'seq' '''')
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
}
}
result3 = result4
if (not (result3 === matcherError)) {
done3 = true
} else {
atPut cursors 1 origCursor3
}
}
if (not done3) {
origCursor4 = (at cursors 1)
result4 = nil
if (not (result4 === matcherError)) {
result5 = (apply this 'seq' '(')
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
}
}
if (not (result4 === matcherError)) {
result5 = (apply this 'gpExpression')
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
}
}
if (not (result4 === matcherError)) {
result5 = (apply this 'seq' ')')
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
}
}
result3 = result4
if (not (result3 === matcherError)) {
done3 = true
} else {
atPut cursors 1 origCursor3
}
}
if (not done3) {
origCursor4 = (at cursors 1)
result4 = nil
if (not (result4 === matcherError)) {
result5 = (apply this 'seq' '{')
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
}
}
if (not (result4 === matcherError)) {
result5 = (apply this 'gpExpression')
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
}
}
if (not (result4 === matcherError)) {
result5 = (apply this 'seq' '}')
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
}
}
result3 = result4
if (not (result3 === matcherError)) {
done3 = true
} else {
atPut cursors 1 origCursor3
}
}
if (not done3) {
origCursor4 = (at cursors 1)
result4 = nil
if (not (result4 === matcherError)) {
origCursor5 = (at cursors 1)
result6 = (apply this 'anything')
result5 = result6
if (result5 === matcherError) {
atPut cursors 1 origCursor5
} else {
c = result5
}
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
}
}
if (not (result4 === matcherError)) {
result5 = (and (c != '}') (c != ')'))
if (not result5) {
result5 = matcherError
}
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
}
}
result3 = result4
if (not (result3 === matcherError)) {
done3 = true
} else {
atPut cursors 1 origCursor3
}
}
if (not done3) {
result3 = matcherError
}
val2 = result3
if (not (val2 === matcherError)) {
add result2 val2
}
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
result1 = result2
return result1
}

method gpExpressionString OMetaParser {
origCursor1 = (at cursors 1)
result1 = nil
if (not (result1 === matcherError)) {
result2 = (apply this 'spaces')
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
origCursor3 = (at cursors 1)
origCursor4 = (at cursors 1)
result4 = nil
if (not (result4 === matcherError)) {
result5 = (apply this 'gpExpression')
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
}
}
result3 = result4
if (result3 === matcherError) {
at cursors 1 origCursor3
} else {
result3 = (toArray (copyList currentList ((at cursors 1) - origCursor3) origCursor3))
}
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
s = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (joinStringArray s)
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
result1 = result2
return result1
}

method rule OMetaParser {
origCursor1 = (at cursors 1)
result1 = nil
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (apply this 'name')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
n = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (list)
val3 = nil
while (not (val3 === matcherError)) {
origCursor4 = (at cursors 1)
result4 = nil
if (not (result4 === matcherError)) {
result5 = (apply this 'token' ':')
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
}
}
if (not (result4 === matcherError)) {
result5 = (apply this 'name')
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
}
}
val3 = result4
if (not (val3 === matcherError)) {
add result3 val3
}
}
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
as = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (apply this 'token' '=')
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (resetTemps this)
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (apply this 'expr')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
e = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (array
                                 'rule' n
                                 (toArray as)
                                 (keys (getField this 'temps'))
                                 e)
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
result1 = result2
return result1
}

method grammar OMetaParser {
origCursor1 = (at cursors 1)
result1 = nil
if (not (result1 === matcherError)) {
result2 = (apply this 'token' 'rule')
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (apply this 'name')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
cls = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (list)
val3 = nil
while (not (val3 === matcherError)) {
result4 = (apply this 'name')
val3 = result4
if (not (val3 === matcherError)) {
add result3 val3
}
}
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
vars = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (apply this 'token' '{')
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (apply this 'rule')
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
r1 = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
result3 = (list)
val3 = nil
while (not (val3 === matcherError)) {
origCursor4 = (at cursors 1)
result4 = nil
if (not (result4 === matcherError)) {
result5 = (apply this 'token' ',')
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
}
}
if (not (result4 === matcherError)) {
result5 = (apply this 'rule')
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
}
}
val3 = result4
if (not (val3 === matcherError)) {
add result3 val3
}
}
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
rs = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (apply this 'token' '}')
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
origCursor2 = (at cursors 1)
origCursor3 = (at cursors 1)
origCursor4 = (at cursors 1)
result4 = nil
if (not (result4 === matcherError)) {
result5 = (list)
val5 = nil
while (not (val5 === matcherError)) {
result6 = (apply this 'anything')
val5 = result6
if (not (val5 === matcherError)) {
add result5 val5
}
}
result4 = result5
if (result4 === matcherError) {
atPut cursors 1 origCursor4
}
}
result3 = result4
if (result3 === matcherError) {
at cursors 1 origCursor3
} else {
result3 = (toArray (copyList currentList ((at cursors 1) - origCursor3) origCursor3))
}
result2 = result3
if (result2 === matcherError) {
atPut cursors 1 origCursor2
} else {
rest = result2
}
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
if (not (result1 === matcherError)) {
result2 = (join (array 'grammar'
                                             cls
                                             (toArray vars)
                                             (joinStringArray rest)
                                             r1)
                                       rs)
result1 = result2
if (result1 === matcherError) {
atPut cursors 1 origCursor1
}
}
result1 = result2
return result1
}


method addTemp OMetaParser t {
  if (temps === nil) {
    temps = (dictionary)
  }
  add temps t
}

method resetTemps OMetaParser {
  temps = (dictionary)
}
