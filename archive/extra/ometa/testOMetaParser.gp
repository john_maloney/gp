g = (new 'GPMatcherParser')

testInput = (letters 'abc')
assert (match g testInput 'name') 'abc' 'abc'

assert (match g testInput 'application') (array 'app' 'abc') 'app abc'

testInput = (letters '~abc')
assert (match g testInput 'expr2') (array 'not' (array 'app' 'abc')) 'not abc'

testInput = (letters '&(abc def)')
assert (match g testInput 'expr2') (array 'lookahead' (array 'and' (array 'app' 'abc') (array 'app' 'def'))) '&(abc def)'

testInput = (letters '~abc*')
assert (match g testInput 'expr3') (array 'many0' (array 'not' (array 'app' 'abc'))) '~abc*'

testInput = (letters 'abc:c')
assert (match g testInput 'expr3') (array 'bind' 'c' (array 'app' 'abc')) 'abc:c'

testInput = (letters '(and (c != '')'') (c != ''}'') ('' '' < c))')
assert (match g testInput 'gpExpressionString') '(and (c != '')'') (c != ''}'') ('' '' < c))' 'gpExpressionString'

testInput = (letters 'abc')
assert (match g testInput 'expr') (array 'and' (array 'app' 'abc')) 'abc'


testInput = (letters 'abc = def')
assert (match g testInput 'rule') (array 'rule' 'abc' (array) (array) (array 'and' (array 'app' 'def'))) 'rule'

testInput = (letters 'abc = def (abc def)')
assert (match g testInput 'rule') (array 'rule' 'abc' (array) (array) (array 'and' (array 'app' 'def') (array 'and' (array 'app' 'abc') (array 'app' 'def')))) 'rule'


testInput = (letters 'rule GPMatcherParser temps {
	expr	= abc+ (def ggg ''a'')*:s
	        | a,
	nn	= expr+
}')

assert (at (match g testInput 'grammar') 1) 'grammar' 'grammar'

testInput = (letters 'rule GPMatcherParser temps {
	args	= ''('' gpExpressionString:g1 ("," gpExpressionString)*:gs ")"
                     -> {(join (array (getField _cl ''g1'')) (getField _cl ''gs''))}
		| ~''('' -> {(array)}
}')

assert (at (match g testInput 'grammar') 1) 'grammar' 'grammar'
