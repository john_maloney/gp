John's GP To Do List

v081
  [ ] build RaspberryPi
  [ ] rebuild ZIP
  [ ] upload & announce

v079
  [ ] fix replicating molecule
  [ ] exit on linux64 gives error
  [ ] high sound latency on Linux
  [ ] test Win with new SDL


[ ] add block to get file/folder name
[ ] add optional alpha to color swatch block
[ ] table: scroll to bottom when new row added
[ ] table: block to return contents of a column

[ ] make block drop in use left side of block
[ ] color picker shouldn't have  transparent option for color slots?
[ ] bug: "share the love" for a script removes the script from original class

[ ] add "pen color" reporter
[ ] suppress "this" as first argument when possible
[ ] "copy class"
[ ] make block drop in use left side of block
[ ] color picker shouldn't have transparent option for color slots? in any case, it should be clickable

[ ] sign GP app to for easier installation

[ ] bug: can delete the "this" block from a method header if the original method name includes a space
[ ] bug: thumbnails disappear after saving a project (texture issue?)
[ ] eric's recording program skipping issue
[ ] get rid of tabs on forever, return, and other blocks that can't have anything after them

[ ] find should filter when not in dev mode (also filter out "my" variable blocks, maybe)
[ ] consider changing GP screen layout (as an option)

[ ] Evelyn's paint editor

[ ] bug: can't copy scripts with methods in them
[ ] bug: errors when editing methods (how to reproduce?)
[ ] html fetch primitive

Projects
  [ ] starters: simplified versions of Clock, SecretMessages
  [ ] starters: interactive piano keyboard
  [ ] starters: madlib
  [ ] possible starter: word counting of Gutenberg books

Build
  [ ] update to latest versions of SDL, portaudio, jpeg, and Cairo on all platforms

Website
  [ ] (later) add thumbnails for team
  [ ] (later) host team bios on gpblocks.org

Performance
  [ ] Rework Block.gp
  [ ] Try: make blocks draw directly rather than caching bitmap costumes
  [ ] Cache stepping morphs? Can we detect structural changes to flush cache?
  [ ] consider replacing SDL2 with bitmap-only graphics
    [ ] evaluate potential performance
    [ ] damage list version of morphic?

Portability
  [ ] Android version (will also work on some Chromebooks)

Media facilities:
  [ ] GIF file reader (writer optional)
  [ ] "copy class"
  [ ] text editor for large amounts of text
  [ ] better paint editor
  [ ] sound recorder/editor
  [ ] camera primitives on all platforms
  [ ] multi-page projects (?)
  [ ] mp3 reader
  [ ] make text rendering treat alpha the same on all platforms

[ ] rework block and blockSpec
[ ] resolve space/tab choice and make uniform (this may be moot soon...)

[ ] remove notch from end of forever and stop all blocks
[ ] make resize handles for scripting area and palette more obvious (?)
[ ] don't allow dropping sprites outside of the stage in user mode
[ ] undelete (undo script deletion)

Rework library to allow GP to evolve itself:
  [ ] make parser capture class and code comments
  [ ] figure out where to store the current "file-level" comments (probably as class comments)
  [ ] organize classes and generic functions into categories
  [ ] category-based system browser

Features:
  [ ] checkbox to show/hide variable monitors
  [ ] block request: timer block like Scratch
  [ ] block request: "go to sprite"

Bugs:
  [ ] bug: pressing the shift-key a lot adds lots of white script cursors (and they don’t easily go away again)
  [ ] bug: In Artillery.gpp, fullscreen, typing in a letter instead of a number into the prompts makes the computer freeze
  [ ] bug: changing a running script leaves the original highlight in a funny state

Linux Tests:

  Ubuntu 12 -- versions of GLib are too old  for release GP; GP built on 12 runs; sound is double steed (maybe ALSA doesn't support 22050?)
  Debian 7 -- versions of GLib are too old
  Debian 8.9 -- GP runs; sound slightly glitchy
  Debian 9 -- did not boot; not supported by VMWare? I may not have installed correctly...
  Fedora 32 bit -- GP runs but with crazy texture artifacts
  Fedora 64 bit -- not supported by VMWare

Done:

Misc
  [x] make instantiate require an explicit class name or class (from a class menu)
  [x] allow "instantiate" to take a class object (Yoshiki)
  [x] make pretty printer not generate \r and remove workaround in Text (which breaks the line wrapping in notes window)
  [x] renew "gpblocks.org" domain name
  [x] request "hyperblocks.org" domain name
  [x] update browser version
  [x] vm: normalize file separaters in fullPath primitive
  [x] change name of 'join strings' to 'join string list _ separator _'
  [x] reorganize variables pane (shared, instance, script variables)
  [x] move "scale" blocks to looks
  [x] button to create an "initialize" method
  [x] better initialization
  [x] paint editor
  [x] save scripts in modules
  [x] bug: "eval (first (parse 'for i 10 { print i }'))" fails
  [x] better handling of Monitor drag/drop
  [x] make save use file name if opened from a file
  [x] use Bresenham trails by default; hide "simulate vector" menu item
  [x] save thumbnail ending in .png in GP project
  [x] save notes at top level of GP project

Variables
  [x] add 'increase shared' block
  [x] separate blocks for each kind of variable

Bugs
  [x] bug: quick click on menu can cause debugger on slow machine (interaction with pop-up help)

BYOB
  [x] 'edit definition' menu command -- what should it do?
  [x] changing block parameters should update block spec
  [x] body of generic function blocks seems to disappear which switching classes
  [x] make it okay for a function/method to have nil body; don't add nop
  [x] bug: inspect (currentTask) fails (due to recursive data structure)

Scaling (e.g. for Ploma)
  [x] better scrolling
  [x] auto-scroll when dragging a block
  [x] cache contents of scripts pane

Extensions
  [x] export script to palette
  [x] import/export extension
  [x] design extension format
  [x] menu command to remove a block from extras category

Debugger
  [x] quick pass at improving debugger error messages
  [x] debugger should not allow script to be changed by dragging blocks (for now)
  [x] debugger highlights multiple blocks with same op name
  [x] simplify debugger in user mode (or use "notifier" like Smalltalk
  [x] make debugger show spec for user-defined functions/methods
  [x] recover from step errors

Recently fixed:

[x] find block button
[x] ability to export sound (or sample array?) as a WAV file
[x] make VM read lib and startup.gp files if not command line args; eliminate shell/batch files
[x] change rotation direction
[x] eyedropper tool for selecting colors
[x] remove support for "when I receive 'initialize'" (convert projects to initialize method)
[x] review/test projects
[x] blocks can stick to the block embedded in the "define" header
[x] improve project saving/opening
  - better dialog
  - separate user, demo, and starter projects
[x] bug: color pickers get stuck on stage
[x] make Windows file primitives handle Unicode
[x] improve iOS version
[x] fix midi and instruments on iOS
[x] remove "speak" blocks
[x] make map and filter return lists not arrays
[x] primitive to list drives on windows for '/'
[x] "computer" shortcut in file dialog
[x] file stream read/write prims
[x] comment block
[x] bug: incorrect argument in user-defined global block with boolean on first call
[x] bug: clear local variable caches when renaming, adding, or removing variables
[x] bug: drag-drop script does not do deep enough copy

[x] show/hide methods (and shared) -- show adds definition to current scripting area
[x] add upload button to browser version
[x] make GP work on 64-bit linux w/o extra libraries
[x] select the imported class after importing it
[x] unify broadcast and send (check existing starters and projects)
[x] add flag to allow reading binary files
[x] faster arrow key scrolling
[x] bug: if a shared method name has a space then calls to it disappear when saving/restoring project
[x] replace "set alpha" block set transparency (0 opaque, 100 fully transparent)

[x] make set/change blocks in palette default to an actual instance var, not "n"
[x] for non-editable menu slots, click anywhere on slot should give menu
[x] version menu item that shows release number
[x] initialize instance vars to 0 (not nil)
[x] bug: Windows delete key does not work in browser
[x] rework method naming scheme

[x] bug: exported app stage size is 800x600 (not 800x500)

[x] Track Wasm
[x] Add JPEG support to browser

