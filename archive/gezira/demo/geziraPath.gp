to randomUniformFill gezira {
  color = (randomColor)
  setField color 'a' (rand 128 200)
  fill = (init (new (at gezira 'UniformColorFill')))
  setColor fill color
  return fill
}

call (function {
  openWindow

  math = (loadModule 'modules/GeziraMath.gp')
  gezira = (loadModule 'modules/GeziraCanvas.gp' math)
  canvas = (init (new (at gezira 'GeziraCanvas')))
  bitmap = (newBitmap 500 500)
  fill bitmap (color 255 255 255)
  setTarget canvas bitmap

  builder = (init (new (at gezira 'PathBuilder')) canvas)

  place builder 80 50
  rt builder 30
  lt builder 30
  rt builder 30 144
  lt builder 30 144
  close builder

  fill = (init (new (at gezira 'UniformColorFill')))
  setColor fill (color 255 0 0)
  setFill canvas fill

  drawPath canvas (result builder)

  fill = (init (new (at gezira 'UniformColorFill')))
  setColor fill (color 128 0 128)
  setFill canvas fill
  setStroke canvas (call (at gezira 'stroke'))
  drawPath canvas (result builder)

  init builder canvas

  place builder 150 250
  lt builder 100 360
  lt builder 0
  fd builder 30
  lt builder 0
  rt builder 70 360
  close builder

  fill = (init (new (at gezira 'UniformColorFill')))
  setColor fill (color 0 255 128 128)
  setFill canvas fill
  setStroke canvas nil
  drawPath canvas (result builder)

  sync canvas

  clearBuffer
  drawBitmap nil bitmap
  flipBuffer

  sleep 10000
})
