(call (function {
  openWindow

  math = (loadModule 'modules/GeziraMath.gp')
  gezira = (loadModule 'modules/GeziraCanvas.gp' math)
  canvas = (init (new (at gezira 'GeziraCanvas')))
  bitmap = (newBitmap 500 500)
  setTarget canvas bitmap

  fill = (init (new (at gezira 'LinearGradientFill')))
  setStart fill 150 150
  setEnd   fill 250 350
  addStop fill (color 255 0 0) 0
  addStop fill (color 0 255 0) 1

  setFill canvas fill
  drawPath canvas (call (at gezira 'rectPath') 150 150 100 200 3)

  fill = (init (new (at gezira 'LinearGradientFill')))
  setStart fill 300 350
  setEnd   fill 200 150
  addStop fill (color 255 0 0) 0
  addStop fill (color 0 255 0) 1

  setFill canvas fill
  drawPath canvas (call (at gezira 'rectPath') 300 150 100 200 3)

  fill = (init (new (at gezira 'LinearGradientFill')))
  setStart fill 160 360
  setEnd   fill 240 440
  addStop fill (color 255 0 0) 0
  addStop fill (color 0 255 0) 1

  setFill canvas fill
  drawPath canvas (call (at gezira 'arcPath') 0 360 200 400 60 40)

  sync canvas

  print (getPixel bitmap 150 150)

  print (getPixel bitmap 249 349)

  print (getPixel bitmap 300 349)
  print (getPixel bitmap 399 150)

  print (getPixel bitmap 200 400)

  clearBuffer
  drawBitmap nil bitmap
  flipBuffer

  sleep 5000
}))