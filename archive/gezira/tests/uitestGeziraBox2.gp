call (function {
gezira = (loadModule 'GeziraCanvas')
canvas = (init (new (at gezira 'GeziraCanvas')))
bitmap = (newBitmap 500 500)
fill bitmap (color 255 255 255)

setTarget canvas bitmap

boxModule = (loadModule 'GeziraBox')
Box = (at boxModule 'Box')

dBox = (init (new Box))
setPosition dBox 200 200
setScale dBox 4.0

checkMarkFill = (init (new (at boxModule 'Fill')))
setFill checkMarkFill (call (at boxModule 'uniformColorFill') (color 255 255 255))
setPath checkMarkFill (load 'icons/checkMark.gp')

shape = (init (new (at boxModule 'Shape')))

fillElem1 = (init (new (at boxModule 'Fill')))
fill1 = (call (at boxModule 'linearGradientFill') 0 0 0 28)
addStop fill1 (color 0 2 4) 0
addStop fill1 (color 0 67 121) 1
setPath fillElem1 (call (at gezira 'rectPath') 0 0 28 28 5)
setFill fillElem1 fill1

fillElem2 = (init (new (at boxModule 'Fill')))
fill2 = (call (at boxModule 'radialGradientFill') 10 -5 28)
addStop fill2 (color 176 220 163) 0
addStop fill2 (color 97 186 70) 1
setPath fillElem2 (call (at gezira 'rectPath') 3 3 22 22 3)
setFill fillElem2 fill2

shape = (init (new (at boxModule 'Shape')))
setElements shape (array fillElem1 fillElem2 checkMarkFill)
setShape dBox shape

topBox = (init (new Box))
setExtent topBox 500 500

topFill = (call (at boxModule 'radialGradientFill') 250 400 250)

addStop topFill (color 10 55 95) 0
addStop topFill (color 3 43 87) 1

setFill (getField topBox 'shape') topFill

addPart topBox dBox

drawOn topBox canvas

openWindow

drawBitmap nil bitmap
flipBuffer
sleep 2000

geziraShutDown
})
