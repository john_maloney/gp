method asGeziraColor Color {
  return (getField (float32Array
     ((alpha this) / 255.0)
     ((red this) / 255.0)
     ((green this) / 255.0)
     ((blue this) / 255.0)) 'data')
}

call (function {

  star = (float32Array
    250.00000 150.00000 237.65650 183.01064 225.31301 216.02128
    225.31301 216.02128 190.10368 217.55979 154.89434 219.09830
    154.89434 219.09830 182.47498 241.03850 210.05562 262.97871
    210.05562 262.97871 200.63855 296.94020 191.22147 330.90169
    191.22147 330.90169 220.61073 311.45084 250.00000 292.00000
    250.00000 292.00000 279.38926 311.45084 308.77852 330.90169
    308.77852 330.90169 299.36144 296.94020 289.94437 262.97871
    289.94437 262.97871 317.52501 241.03850 345.10565 219.09830
    345.10565 219.09830 309.89631 217.55979 274.68698 216.02128
    274.68698 216.02128 262.34349 183.01064 250.00000 150.00000
)

gezira = (loadModule 'Gezira')
bitmap = (newBitmap 500 500)
img = (allocateGeziraImage bitmap)

call (at gezira 'initGezira')

t = (init (new (at gezira 'TransformBeziers')))
setM11 t 1.0
setM22 t 1.0
setM31 t 0
setM32 t 0

c = (init (new (at gezira 'ClipBeziers')))
setMinx c 0
setMiny c 0
setMaxx c 500
setMaxy c 500

r = (init (new (at gezira 'Rasterize')))
a = (init (new (at gezira 'ApplyTexture')))

comp = (init (new (at gezira 'CompositeTextures')))

grad = (init (new (at gezira 'LinearGradient')))
setSx grad 200
setSy grad 200
setEx grad 300
setEy grad 300

ext = (init (new (at gezira 'ReflectGradient')))

apply = (init (new (at gezira 'ApplyColorSpans')))

span = (init (new (at gezira 'ColorSpan')))
setField span 'vS1' (asGeziraColor (color 255 0 0 255))
setField span 'vS2' (asGeziraColor (color 0 255 0 255))
setL span 1

setSpans apply (array span)

t1 = (array grad ext apply)

t2 = (init (new (at gezira 'ReadFromImageARGB32')))
setImage t2 img

//t1 = (init (new (at gezira 'ReadFromImageARGB32')))
//bit3 = (newBitmap 500 500)
//fill bit3 (color 255 0 255 150)
//setImage t1 (allocateGeziraImage bit3)

setT1 comp t1
setT2 comp t2
setC comp (init (new (at gezira 'CompositeOver')))
setT a comp

w = (init (new (at gezira 'WriteToImageARGB32')))
setImage w img

openWindow

  fill bitmap (color 0 0 0 0)

  pipe = (geziraPipeline t c r a w)
  geziraFeed pipe (getField star 'data')
  geziraSync

  clearBuffer
  drawBitmap nil bitmap
  flipBuffer
  sleep 2000

geziraShutDown
})
