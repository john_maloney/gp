call (function {
boxModule = (loadModule 'GeziraBox')
Box = (at boxModule 'Box')

topBox = (init (new Box))
setExtent topBox 500 500

aBox = (init (new Box))

setPosition aBox 100 50
setExtent aBox 200 200
setScale aBox 0.7
addPart topBox aBox

setCenter aBox (init (new (at boxModule 'Point')) 250 250)
assert (toString (position aBox)) '(180.0, 180.0)' 'because it is scaled'

assert (left aBox) 180.0 'left 0.7'
// print (right aBox) 320.0 'right 0.7'
// print (top aBox) 320.0 'top 0.7'
// print (bottom aBox) 180.0 'bottom 0.7'

setScale aBox 1.0

assert (left aBox) 180.0 'left 1.0'
// print (right aBox) 380.0 'right 1.0'
// print (top aBox) 380.0 'top 1.0'
// print (bottom aBox) 180.0 'bottom 1.0'

})
