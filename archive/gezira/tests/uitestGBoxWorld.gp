call (function {
boxModule = (loadModule 'GeziraBox')
Box = (at boxModule 'Box')

topBox = (init (new Box))
setExtent topBox 500 500

aBox = (init (new Box))

setPosition aBox 100 50
setExtent aBox 200 200
setScale aBox 0.7
addPart topBox aBox

bBox = (init (new Box))
setPosition bBox 100 180
addPart aBox bBox

cBox = (init (new Box))
setPosition cBox 170 -100
addPart bBox cBox

setRotation bBox 30

a = (call (at boxModule 'openBoxAsWindow') topBox)
startStepping a
})
