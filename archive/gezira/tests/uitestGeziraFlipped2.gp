method asGeziraColor Color {
  return (getField (float32Array
     ((alpha this) / 255.0)
     ((red this) / 255.0)
     ((green this) / 255.0)
     ((blue this) / 255.0)) 'data')
}

call (function {

gezira = (loadModule 'Gezira')
Matrix = (at gezira 'Matrix')
bitmap = (newBitmap 500 500)
img = (allocateGeziraImage bitmap)


box = (float32Array 0 0 0 0 200 0
                    200 0 200 0 200 200
                    200 200 200 200 0 200
                    0 200 0 200 0 0)

call (at gezira 'initGezira')

trans = (beIdentity (init (new Matrix)))
setRotation trans 30
setTranslation trans 100 100

t = (init (new (at gezira 'TransformBeziers')))
setField t 'vM' (data trans)

c = (init (new (at gezira 'ClipBeziers')))
setMinx c 0
setMiny c 0
setMaxx c 500
setMaxy c 500

r = (init (new (at gezira 'Rasterize')))
a = (init (new (at gezira 'ApplyTexture')))

comp = (init (new (at gezira 'CompositeTextures')))

gp = (readFrom (new 'PNGReader') (readFile 'tests/gp.png' true))
inv = (init (new (at gezira 'TransformPoints')))
inverseMatrix2x3Into (data trans) (getField inv 'vM')

t1 = (init (new (at gezira 'ReadFromImageARGB32Flipped')))
setImage t1 (allocateGeziraImage gp)

t2 = (init (new (at gezira 'ReadFromImageARGB32Flipped')))
setImage t2 img

setT1 comp (array inv t1)
setT2 comp t2
setC comp (init (new (at gezira 'CompositeOver')))
setT a comp

w = (init (new (at gezira 'WriteToImageARGB32Flipped')))
setImage w img

openWindow

  fill bitmap (color 255 255 255)

  pipe = (geziraPipeline t c r a w)
  geziraFeed pipe (getField box 'data')
  geziraSync

  clearBuffer
  drawBitmap nil bitmap
  flipBuffer
  sleep 2000

geziraShutDown

})
