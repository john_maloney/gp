// High Resolution Timer (Mac OS X Only)
// John Maloney, November 2013

#include <mach/mach_time.h>

double conversion_factor;
uint64_t machStartTime;

static inline void initNanosecTimer() {
	mach_timebase_info_data_t timebase;
	mach_timebase_info(&timebase);
	conversion_factor = (double) timebase.numer / (double) timebase.denom;
}

static inline void nanosecsReset(void) { machStartTime = mach_absolute_time(); }

static inline double nanosecs(void) {
	uint64_t now = mach_absolute_time();
	double nanoSecs = (double) (now - machStartTime) * conversion_factor;
	machStartTime = mach_absolute_time();
	return nanoSecs;
}
